2016-02-25
Har skapat en (igentligen tv�) ny fiende genom att l�gga till tv� enums och tv� till cases i fiendeklassens switch-case som best�mmer vad den �r f�r n�got 
baserad p� den enum den f�r n�r den skapas. Fienden som jag hade skapat �r ett kluster fiende som skapar tv� till fiender n�r den f�rst�rs. F�r att se till
att detta skulle fungera och inte vara laggit eller f�rst�ra en vector-loop s� sa jag till EnemyManagern att skapa tv� fiender med enumet SHIP_TYPE4 f�r 
varje fiende med enumet SHIP_TYPE3 som skapas samt gav alla fiender en bool med namnet isActive som best�mmer om de ritas ut och uppdateras eller inte beroende
p� om den �r true eller false. I IsHit functionen i EnemyManagern s� hade jag nestlat en vector iterator loop i den f�rsta vector iterator loopen som d� letar
efter de tv� f�rsta instanserna av fiender med SHIP_TYPE4 enumet och kallar deras Activate funktion s� att de d� ritas ut och updateras. Efter att den hade 
aktiverat tv� fiender s� hoppar den ut ur den while-loopen och �terv�nder till d�r den var i den f�rsta while-loopen med hj�lp utav goto kommandot som, enligt
google, var den enda riktiga anv�ndningen som goto hade.