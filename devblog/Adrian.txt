-------------------------
2016-02-16 @ 16:30

Jag gjorde så att går att skjuta ner fiendeskepp med projektilerna. Jag tog den
snabba vägen ut och gav både Projectile- och EnemyShip-klasserna en getCollider
funktion som returnerar en sf::CircleShape som representerar objektens kollison.
Ifall vi kommer att behöva/vill ha mer exakt kollision så går det rätt så lätt
modifiera den nuvarande koden så att getCollider returnerar en bättre form för
objektet och sen lägga till en passande kollisionsfunktion i CollisionManager
för att sedan använda den där kollisionerna emellan skotten och fienderna kollas
i EnemyManager::IsHit().

Dessutom så implementerade jag gränserna för planeten och gjorde att spelaren
och fienderna inte kan flytta sig över dem. Just nu så renderas gränserna som
bara ett par svarta transparenta rektanglar, men det lär vi ändra senare.
Sättet jag begränsar deras rörelser är helt enkelt att flytta tillbaka
spelaren/fienden om deras position är mindre/högre (beroende på om det är den
vänstra/högra gränsen) än gränsens position. Ett problem som uppstådde var att
så som vi hade satt upp det så när man gick under 0 grader så kom man till 360
och vice versa. Om man då hade t.ex. den vänstra gränsen på 0 grader, går över
den, hamnar på position -1 som då omvandlas till 359 grader och checken om man
har åkt över gränsen säger nej, det har man inte, då 359 är > 0.

Jag löste det genom att vänta med att omvandla grader mindre än 0 och mer än 360
grader tills mindre än -360 och mer än 740 grader. Detta spelar ingen roll för
positioneringen, eftersom för trigonometrin så är t.ex. -180 och 180 grader
samma sak. Jag har inte märkt några problem med den här lösningen än, men vi får
se om det dyker upp något senare.

-------------------------
2016-02-18 @ 23:30

Man glömde gårdagens blogg, men jag fick inte alltför mycket gjort på projektet
då iaf, det mesta jag gjorde var merga ihop saker från olika branches, fixa lite
struktur och jag hjälpte till med de andras saker som de strulade med.

Idag så fick jag en hel del gjort för att få spelet klart inför alpha-deadlinen.
Jag gjorde så att man förlorar 'market value' (pengar och hälsa) när man spawnar
turrets och så att det inte går att placera ut flera turrets på varandra. Nu när
fiendeskeppen landar på planeten så förlorar man också 'market value'. Jag bytte
även ut en hel del temporära sprites till sprites som grafikerna har gjort. Jag
hjälpte till Karl med att skapa följande missiler och implementerade powerup'en
som spawnar dem. Jag borde verkligen skriva mer om hur och varför, men jag är
alldeles för trött för att skriva något mycket just nu, får fylla på i
morgondagens post, då jag planerar att ta det någorlunda lugnt imgoron och
kommer nog inte att ha alltför mycket att skriva annars då.

-------------------------
2016-02-26 @ 20:00

Fyfan vad dålig man har vart att hålla den här uppdaterad, så här får det bli en
lång uppdatering för hela veckan:

I måndags så skapade jag huvudmenyn. Det var inget riktigt speciellt, eftersom
jag gjorde bara klassen och att den fungerade grundligt, då det var lite problem
med de sprites vi hade och jag ville inte göra något alltför komplicerat ifall
att jag var tvungen att ändra på något när sprites'en blev fixade.

I tisdags så blev alla menysprites fixade, så jag tog och implementerade dem
samt skapade ordentliga klasser för knapparna, då jag tidigare bara hade använt
rektanglar för att kolla vilken knapp man hovrar över med musen. Knapparna kan
tekniskt sett användas utan problem vart som helst I spelet så som de är nu, men
visuellt så lär de nog behövas anpassas innan de används någon annan stans. Jag
skulle vilja kalla menyn färdig, men det finns säkert nånting som skulle kunna
putsas. Jag flyttade centern på spelaren från bilen till pansarvagnen, vilket
krävde inget mer än lite modifikation på vilken jag satte i centrum och vilken
jag räknade utifrån det.

I onsdags så la jag till en ny fiende. Just nu så ligger den i samma klass som
den förra och de skiljs åt via en switch i konstruktorn. Det fungerar eftersom
det inte är någon stor skillnad alls emellan dom två fienderna, allt som behövs
är att ändra på ett antal variabler som lätt görs i switch-satsen. Men det lär
nog vara smartare att göra om den till en separat klass senare, då vi kanske
kommer att vilja lägga till något på den ena fienden men inte den andra. Utöver
det så gjorde jag även så att fienderna kan nu tåla mer än en träff och de kan
"skada" planeten olika mycket. Hälsan löste jag genom att lägga till
'void hit(float dmg)' funktionen till fiendeklassen samt en 'hp' variabel samt
'bool isDestroyed()' som returnerar true om fiendens hp är <= 0. För skadan som
fienderna gör så lade jag till en variabel som är skeppets styrka samt en
funktion som returnerar den. Utöver det så fixade jag en del småbuggar.

Igår (torsdags) så arbetade jag på spelets HUD. Jag blev inte klar med den, men
den största utmaningen med den blev mer eller mindre färdig: HP mätaren som
visas som en graf. Det sättet som jag först hade tänkt att lösa det var genom
att spara det nuvarande och föregående hälsovärdet i en array/vektor och
därefter helt enkelt rita graf emellan punkterna, där x-axeln är tid och
y-värdet är hälsan. Det första problemet var att SFML har inget klass
eller funktion för linjer emellan två punkter, jag citerar deras dokumentation:
"There's no shape class for lines. The reason is simple: If your line has a
thickness, it is a rectangle.". Så det var det jag gjorde, en funktion som ritar
skapar en sf::RectangleShape emellan två angivna punkter som har en angiven
bredd. Men eftersom det inte skulle vara optimalt att skapa nya "linjer" varje
gång grafen uppdateras så bestämde jag mig för att istället spara en vektor med
alla tidigare hälsovärden så behåller jag istället en vektor med mina linjer som
jag flyttar "bakåt" i grafen varje gång den uppdateras, och ta bort de linjer
som inte kommer att ritas ut.

Idag (fredag) så blev det inte alltför mycket gjort, då det mesta av tiden gick
till de två föreläsningar vi hade och vårt scrumm möte. Därefter så arbetade jag
endast lite grann på spelet. Det jag gjorde var att jag separerade planeten och
himlen till två separata klasser, då vi vill kunna rita ut saker emellan dem.
Jag började också på molnen som jag planerar att bli klar med nån gång under
helgen.

-------------------------
2016-03-01 @ 22:00

Igår skrev jag inget eftersom jag gjorde inget märkvärdigt, då vi hade
playtesting nästan hela dagen.

Idag så implementerade jag äntligen de flesta animationer som vi har haft redo
ett tag nu. Jag gjorde det genom att skapa en ny klass 'AnimatedSprite' som
ärver 'sf::Sprite'. Det enda som den lägger till är tre funktioner: En som tar
emot en vektor med sf::IntRects som visar var alla frames finns i sf::Sprite'ns
textur. Den andra funktionen sätter animiationens hastighet och till sist en
funktion som uppdaterar det hela varje frame för att spela animationen.

Det som saknas, som jag skulle vilja fixa förr eller senare är att kunna ladda
in alla koordinater för varje frame från en xml fil eller liknande. Det ändrar
inte på någon funktionalitet, men man slipper sätta koordinaterna manuellt
vilket kan vara tidskrävande beroende på antalet frames.

Det andra jag gjorde var att fixa så att fienderna kommer i ordentliga vågor
och så att sektorer låses upp efter ett par antal fiendevågor. Tidigare så har
alla fiender vart i en enda stor 'våg' med lite små pauser då och då. Nu så
startar en våg, när alla fiender är döda så startar den en ny våg efter 10 sek.
Just nu är fiendetyperna och deras position slumpade och antalet fiender per våg
beror på vilken våg det är, men det bör vara enkelt att skapa "scriptade" vågor
senare när vi är klara med att designa dem.

Utöver det så har jag bytt ut sprites, fixat småsaker som följande missiler som
cirkulerar fiender istället för att träffa dem och lagt in effekten för EMP
powerupen.

-------------------------
2016-03-02 @ 23:30

Det mesta jag gjorde idag var att implementera fler animationer som vi har haft
liggandes som explosioner, muzzleflash och dammoln när skepp landar. Jag
spenderade också en hel del tid på att fixa en bugg med lerp'en vi gör på
kameran och wrappingen som finns på spelarens rotation. Buggen gjorde så att om
man går över toppen på planeten så gör kameran en snabb disorienterande snurr.
Jag hade redan löst det här tidigare, men då var det för missilens rotation mot
fienden den följer och jag vet inte riktigt varför men jag hade en del problem
att implementera samma lösning som gjorde att de tog mer tid än vad det borde ha
gjort.

Jag la också till en boost för spelaren vilket tog bokstavligen någon minut,
jag multiplicerar spelarens hastighet med ca 2.4 om man håller i vänstra shift.
Jag behöver fortfarande implementera animationerna vi har för boosten dock.

Jag hjälpte även till med Karls highscore-meny, mer specifikt att spara ner en
uppdaterad lista ner till en fil och en del problem som kom med det.

-------------------------
2016-03-03 @ 16:16

Idag så skapade jag en minimap som visar vart på planeten man är och vart alla
fiender kommer ifrån samt hur nära de är. Den får spelarens och fiendernas
position genom att spelaren och enemymanagern har en referens till HUD'en och
skickar positionerna via den. Det som inte syns på minimapen just nu är alla
turrets positioner och vart gränserna finns. Det borde inte vara svårt att få
alla turrets att visas upp, men gränserna lär bli svårare om vi vill också att
det ska se bra ut, vi kan säkert klura ut något men jag vet inte hur just nu.

Förutom det vanliga byta ut gamla texturer till nya och mindre tweaks på
småsaker så har jag inte gjort något mer, minimapen tog en hel del tid.
