#include "Sky.h"
#include <SFML/Graphics/RenderTarget.hpp>

Sky::Sky()
{
	skyTexture.loadFromFile("../assets/sprites/space.png");
	skySprite1.setTexture(skyTexture);
	cloudTexture.loadFromFile("../assets/planet/moln Sheet.png");
	//cloudSprite.setTexture(cloudTexture);
	{
		auto size = skySprite1.getLocalBounds();
		skySprite1.setOrigin(0, size.height);
		skySprite1.setPosition(0, 0);
	}
	/*{
	auto size = cloudSprite.getLocalBounds();
	cloudSprite.setOrigin(size.width / 2, size.height / 2);
	cloudSprite.setPosition(0, 0);
	}*/
	skySprite2 = sf::Sprite(skySprite1);
	skySprite2.setScale(1, -1);
	skySprite3 = sf::Sprite(skySprite1);
	skySprite3.setScale(-1, -1);
	skySprite4 = sf::Sprite(skySprite1);
	skySprite4.setScale(-1, 1);
}

Sky::~Sky()
{
}

void Sky::Update(float dt)
{
	skySprite1.rotate(-.3 * dt);
	skySprite2.rotate(-.3 * dt);
	skySprite3.rotate(-.3 * dt);
	skySprite4.rotate(-.3 * dt);
}

void Sky::setPosition(float x, float y)
{
	sf::Vector2f pos = sf::Vector2f(x, y);
	setPosition(pos);
}

void Sky::setPosition(sf::Vector2f& pos)
{
	sf::Transformable::setPosition(pos);
	skySprite1.setPosition(pos);
	skySprite2.setPosition(pos);
	skySprite3.setPosition(pos);
	skySprite4.setPosition(pos);
}

void Sky::setScale(float x, float y)
{
	sf::Vector2f scale = sf::Vector2f(x, y);
	setScale(scale);
}

void Sky::setScale(sf::Vector2f& scale)
{
	sf::Transformable::setScale(scale);
	skySprite1.setScale(scale.x, scale.y);
	skySprite2.setScale(scale.x, -scale.y);
	skySprite3.setScale(-scale.x, -scale.y);
	skySprite4.setScale(-scale.x, scale.y);
}

void Sky::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(skySprite1, states);
	target.draw(skySprite2, states);
	target.draw(skySprite3, states);
	target.draw(skySprite4, states);
}
