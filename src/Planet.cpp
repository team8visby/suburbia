#include "Planet.h"
#include <SFML/Graphics/RenderTarget.hpp>

Planet::Planet()
{
	planetTexture.loadFromFile("../assets/Sprites/planet.png");
	planetShape1.setTexture(planetTexture);
	{
		auto size = planetShape1.getLocalBounds();
		planetShape1.setOrigin(size.width / 2, size.height / 2);
		planetShape1.setPosition(0, 0);
	}
	/*skyScale = 1.4;
	planetShape2 = sf::Sprite(planetShape1);
	planetShape2.setScale(1, -1);
	planetShape3 = sf::Sprite(planetShape1);
	planetShape3.setScale(-1, -1);
	planetShape4 = sf::Sprite(planetShape1);
	planetShape4.setScale(-1, 1);*/

	bordersEnabled = true;
	float borderWidth = 2048;
	float borderHeight = 4096;
	ccwBorder.setSize(sf::Vector2f(borderWidth, borderHeight));
	ccwBorder.setOrigin(borderWidth, borderHeight);
	ccwBorder.setPosition(0, 0);
	ccwBorder.setRotation(0);
	ccwBorder.setFillColor(sf::Color(0, 0, 0, 200));

	cwBorder.setSize(sf::Vector2f(borderWidth, borderHeight));
	cwBorder.setOrigin(0, borderHeight);
	cwBorder.setPosition(0, 0);
	cwBorder.setRotation(90);
	cwBorder.setFillColor(sf::Color(0, 0, 0, 200));
}

Planet::~Planet()
{
}

void Planet::Update(float dt)
{
	
}

void Planet::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(planetShape1, states);
	//target.draw(planetShape2, states);
	//target.draw(planetShape3, states);
	//target.draw(planetShape4, states);
	//target.draw(cloudSprite, states);
	if (bordersEnabled)
	{
		target.draw(ccwBorder, states);
		target.draw(cwBorder, states);
	}
}

void Planet::setBorders(float cwAngle, float ccwAngle)
{
	if (cwAngle >= 720)
	{
		cwAngle -= 360;
	}
	else if (cwAngle < -360)
	{
		cwAngle += 360;
	}

	if (ccwAngle >= 720)
	{
		ccwAngle -= 360;
	}
	else if (ccwAngle < -360)
	{
		ccwAngle += 360;
	}

	if (bordersEnabled)
	{
		cwBorder.setRotation(cwAngle);
		ccwBorder.setRotation(ccwAngle);
	}
}

void Planet::setBordersEnabled(bool enabled)
{
	bordersEnabled = enabled;
}

void Planet::setPosition(float x, float y)
{
	sf::Transformable::setPosition(x, y);
	planetShape1.setPosition(x, y);
}

void Planet::setPosition(sf::Vector2f pos)
{
	setPosition(pos.x, pos.y);
}

void Planet::setScale(sf::Vector2f scale)
{
	setScale(scale.x, scale.y);
}

void Planet::setScale(float x, float y)
{
	sf::Transformable::setScale(x, y);
	planetShape1.setScale(x, y);
}

void Planet::rotate(float angle)
{
	sf::Transformable::rotate(angle);
	planetShape1.rotate(angle);
}

void Planet::setRotation(float angle)
{
	sf::Transformable::setRotation(angle);
	planetShape1.setRotation(angle);
}

float Planet::getRadiusSize()
{
	return 1400 * getScale().x;
}

float Planet::getCwBorder()
{
	return cwBorder.getRotation();
}

float Planet::getCcwBorder()
{
	return ccwBorder.getRotation();
}

bool Planet::isBordersEnabled()
{
	return bordersEnabled;
}
