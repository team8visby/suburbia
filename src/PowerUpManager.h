#pragma once
#include "stdafx.h"
//#include "PowerUp.h"

class MiniMap;
class PowerUp;
class Player;
class Crate;

enum POWERTYPE
{
	POWER_NONE,
	POWER_ROCKETS,
	POWER_EMP,
	POWER_FREEZE
};

class PowerUpManager
{
	std::vector<PowerUp*> vecPowerUp;
	std::vector<Crate*> vecCrate;
	POWERTYPE storedPowerType;
	sf::SoundBuffer buffer;
	sf::Texture powerupTexture;
	sf::Texture crateTexture;
	sf::Sound pickupSFX;
	MiniMap * miniMap;
	bool firstPower;
public:
	PowerUpManager(MiniMap * map);
	~PowerUpManager();
	void ClearPowerUp();
	void CreatePowerup(POWERTYPE getType, float getRelativeDegree, float getRelativeRadius);
	void Draw(sf::RenderWindow* window);
	//TODO: add more neccesary functions
	POWERTYPE GetPowerUp();
	void Update(float deltaTime);
	void StorePowerUp(Player* player);
	bool CheckFirstPower();
};
