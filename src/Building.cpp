#include "stdafx.h"
#include "Building.h"
#include "BuildingManager.h"

Building::Building(sf::Texture &acqTexture, BUILDINGTYPE getType, sf::Vector2f getPosition, float getRotation)
{
	buildingType = getType;
	damageType = NO_DAMAGE;
	mySprite.setTexture(acqTexture);
	switch (getType)
	{
	case TYPE1:
		mySprite.setTextureRect(sf::IntRect(0, 0, 276, 79));
		break;
	case TYPE2:
		mySprite.setTextureRect(sf::IntRect(279, 0, 159, 130));
		break;
	case TYPE3:
		mySprite.setTextureRect(sf::IntRect(349, 133, 124, 72));
		break;
	case TYPE4:
		mySprite.setTextureRect(sf::IntRect(0, 245, 180, 85));
		break;
	case TYPE5:
		mySprite.setTextureRect(sf::IntRect(177, 333, 169, 92));
		break;
	case TYPE6:
		mySprite.setTextureRect(sf::IntRect(0, 158, 273, 156));
		break;
	case TYPE7:
		mySprite.setTextureRect(sf::IntRect(0, 316, 263, 160));
		break;
	case TYPE8:
		mySprite.setTextureRect(sf::IntRect(462, 0, 170, 189));
		break;
	case TYPE9:
		mySprite.setTextureRect(sf::IntRect(275, 0, 193, 176));
		break;
	case TYPE10:
		mySprite.setTextureRect(sf::IntRect(265, 411, 257, 93));
		break;
	}
	
	//mySprite.setTexture(myTexture);
	setPosition(getPosition);
	setRotation(getRotation);
	auto rect = mySprite.getGlobalBounds();
	mySprite.setOrigin(rect.width/2, rect.height - rect.width * 0.1);
	mySprite.setPosition(getPosition);
	mySprite.setRotation(getRotation + 90);
}

Building::~Building()
{
}

void Building::Update(float deltaTime)
{
}

void Building::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.draw(mySprite, states);
}

void Building::SetDamageType(DAMAGESTATE getState)
{
	damageType = getState;
	if (damageType == DAMAGED)
	{
		switch (buildingType)
		{
		case TYPE1:
			mySprite.setTextureRect(sf::IntRect(0, 82, 276, 79));
			break;
		case TYPE2:
			mySprite.setTextureRect(sf::IntRect(183, 164, 163, 134));
			break;
		case TYPE3:
			mySprite.setTextureRect(sf::IntRect(349, 133, 124, 72));
			break;
		case TYPE4:
			mySprite.setTextureRect(sf::IntRect(0, 164, 356, 182));
			break;
		case TYPE5:
			mySprite.setTextureRect(sf::IntRect(0, 333, 174, 92));
			break;
		case TYPE6:
			mySprite.setTextureRect(sf::IntRect(0, 0, 273, 156));
			break;
		case TYPE7:
			mySprite.setTextureRect(sf::IntRect(0, 478, 263, 159));
			break;
		case TYPE8:
			mySprite.setTextureRect(sf::IntRect(460, 506, 171, 182));
			break;
		case TYPE9:
			mySprite.setTextureRect(sf::IntRect(265, 506, 193, 165));
			break;
		case TYPE10:
			mySprite.setTextureRect(sf::IntRect(265, 411, 257, 93));
			break;
		}
	}
}

DAMAGESTATE Building::getDamageType()
{
	return damageType;
}

sf::CircleShape Building::GetCollider()
{
	sf::CircleShape collider;
	collider.setPosition(getPosition());
	collider.setRadius(mySprite.getLocalBounds().height);
	return collider;
}
