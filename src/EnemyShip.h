#pragma once
#include "stdafx.h"
#include "AnimatedSprite.h"

class Planet;

enum SHIPTYPE;


class EnemyShip : public sf::Transformable, public sf::Drawable
{
	sf::Sprite ptrSprite;
	sf::Sprite sprite2;
	sf::Texture myTexture;
	AnimatedSprite iceCube;
	float wiggleSpeed;
	float wiggleSize;
	float myOffset;
	float relativeAngle;
	float relativeRadius;
	SHIPTYPE myType;
	int health;
	int cost;
	float descentSpeed;
	bool landed;
	Planet* planet;
	bool isActive;
	int attachedShips;
	AnimatedSprite shield;
	bool isFrozen;
	float freezeTimer;

public:
	EnemyShip(sf::Texture &acqTexture, sf::Texture &iceTexture, Planet* planet, SHIPTYPE getType, float getOffset, float getRelativeDegree, float getRelativeRadius);
	~EnemyShip();
	void Update(float dt);
	float GetRelativeAngle();
	float GetRelativeRadius();
	SHIPTYPE GetType();
	void hit(int damage);
	void destroy();
	bool isDestroyed();
	int getCost();
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	sf::CircleShape getCollider();
	bool hasLanded();
	void Activate(float acqAngle, float acqRadius);
	bool IsActive();
	bool IsFrozen();
	void SetFreezeTimer(float time);
	float GetFreezeTimer();
};
