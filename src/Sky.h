#pragma once
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>

class Sky : public sf::Transformable, public sf::Drawable
{
public:
	Sky();
	~Sky();
	void Update(float dt);
	void setPosition(float x, float y);
	void setPosition(sf::Vector2f& pos);
	void setScale(float x, float y);
	void setScale(sf::Vector2f& scale);
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
private:
	sf::Texture cloudTexture;
	sf::Texture skyTexture;
	sf::Sprite skySprite1;
	sf::Sprite skySprite2;
	sf::Sprite skySprite3;
	sf::Sprite skySprite4;
	std::vector<sf::Sprite> clouds;
};
