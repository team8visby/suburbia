#include "FreezeShot.h"

FreezeShot::FreezeShot(sf::Vector2f pos, float rotation, EnemyManager* enemyManager, sf::Texture &projtext): Projectile(pos, rotation, projtext)
{
	freezeSprite.setTexture(projtext);
	std::vector<sf::IntRect> freezFrames;
	freezFrames.push_back(sf::IntRect(0, 0, 33, 120));
	freezFrames.push_back(sf::IntRect(34, 0, 33, 120));
	freezFrames.push_back(sf::IntRect(68, 0, 33, 120));
	freezFrames.push_back(sf::IntRect(102, 0, 33, 120));
	freezFrames.push_back(sf::IntRect(136, 0, 33, 120));
	freezFrames.push_back(sf::IntRect(170, 0, 33, 120));
	freezeSprite.setAnimationRects(freezFrames);
	freezeSprite.setLoopEnabled(true);
	freezeSprite.setFrameRate(8);
	freezeSprite.setTextureRect(sf::IntRect(0, 0, 29, 94));
	freezeSprite.setOrigin(freezeSprite.getLocalBounds().width / 2, 0);
	freezeSprite.setPosition(pos);
	freezeSprite.setRotation(rotation + 90);
}

void FreezeShot::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(freezeSprite, states);
}

void FreezeShot::Update(float dt)
{
	Projectile::Update(dt);
	freezeSprite.setPosition(getPosition());
	freezeSprite.updateAnimation(dt);
}

int FreezeShot::getStrenght()
{
	return 1;
}

PROJECTILETYPE FreezeShot::GetProjectileType()
{
	return PROJ_FREEZE;
}
