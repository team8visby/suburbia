#include "stdafx.h"
#include "Boss.h"
#include "Gamestate.h"

Boss::Boss()
{
	
	bossTalk = false;
	myTexture.loadFromFile("../assets/Sprites/TheBossWithBeard_test1.png");
	mySprite.setTexture(myTexture);
	angryTexture.loadFromFile("../assets/Sprites/idle_arg.png");
	angryAnimation.setTexture(angryTexture);
	angryStartTexture.loadFromFile("../assets/Sprites/inzoomning_arg.png");
	angryEndTexture.loadFromFile("../assets/Sprites/utzoomning_arg.png");
	angryStartAnimation.setTexture(angryStartTexture);
	angryEndAnimation.setTexture(angryEndTexture);
	normalTexture.loadFromFile("../assets/Sprites/Idle_lugn.png");
	happyTexture.loadFromFile("../assets/Sprites/idle_glad.png");
	normalAnimation.setTexture(normalTexture);
	happyAnimation.setTexture(happyTexture);
	std::vector<sf::IntRect> normalAni;
	normalAni.push_back(sf::IntRect(0, 0, 393, 540));
	normalAni.push_back(sf::IntRect(790, 0, 393, 542));
	normalAni.push_back(sf::IntRect(1185, 558, 393, 545));
	normalAni.push_back(sf::IntRect(1185, 0, 393, 556));
	normalAni.push_back(sf::IntRect(790, 1136, 393, 582));
	normalAni.push_back(sf::IntRect(790, 544, 393, 590));
	normalAni.push_back(sf::IntRect(1185, 1105, 393, 595));
	normalAni.push_back(sf::IntRect(395, 623, 393, 615));
	normalAni.push_back(sf::IntRect(395, 0, 393, 621));
	normalAni.push_back(sf::IntRect(0, 542, 393, 634));
	normalAnimation.setAnimationRects(normalAni);
	normalAnimation.setFrameRate(20);
	//normalAnimation.setLoopEnabled(false);
	std::vector<sf::IntRect> happyAni;
	happyAni.push_back(sf::IntRect(0, 0, 393, 541));
	happyAni.push_back(sf::IntRect(790, 0, 393, 543));
	happyAni.push_back(sf::IntRect(1185, 559, 393, 546));
	happyAni.push_back(sf::IntRect(1185, 0, 393, 557));
	happyAni.push_back(sf::IntRect(790, 1138, 393, 583));
	happyAni.push_back(sf::IntRect(790, 545, 393, 591));
	happyAni.push_back(sf::IntRect(1185, 1107, 393, 596));
	happyAni.push_back(sf::IntRect(395, 624, 393, 616));
	happyAni.push_back(sf::IntRect(395, 0, 393, 622));
	happyAni.push_back(sf::IntRect(0, 543, 393, 637));
	happyAnimation.setAnimationRects(happyAni);
	happyAnimation.setFrameRate(20);
	//happyAnimation.setLoopEnabled(false);
	std::vector<sf::IntRect> angryStartAni;
	angryStartAni.push_back(sf::IntRect(0, 1844, 1, 1));
	angryStartAni.push_back(sf::IntRect(1293, 346, 214, 227));
	angryStartAni.push_back(sf::IntRect(1293, 0, 296, 344));
	angryStartAni.push_back(sf::IntRect(682, 0, 609, 589));
	angryStartAni.push_back(sf::IntRect(1271, 1255, 574, 587));
	angryStartAni.push_back(sf::IntRect(654, 1255, 615, 571));
	angryStartAni.push_back(sf::IntRect(0, 1231, 652, 588));
	angryStartAni.push_back(sf::IntRect(0, 0, 680, 604));
	angryStartAni.push_back(sf::IntRect(0, 606, 666, 623));
	angryStartAni.push_back(sf::IntRect(668, 606, 618, 647));
	angryStartAni.push_back(sf::IntRect(1288, 591, 391, 539));
	angryStartAnimation.setAnimationRects(angryStartAni);
	angryStartAnimation.setFrameRate(20);
	angryStartAnimation.setLoopEnabled(false);
	std::vector<sf::IntRect> angryEndAni;
	angryEndAni.push_back(sf::IntRect(0, 1048, 384, 504));
	angryEndAni.push_back(sf::IntRect(888, 1025, 390, 507));
	angryEndAni.push_back(sf::IntRect(894, 0, 417, 536));
	angryEndAni.push_back(sf::IntRect(457, 0, 425, 506));
	angryEndAni.push_back(sf::IntRect(0, 0, 455, 509));
	angryEndAni.push_back(sf::IntRect(0, 511, 453, 535));
	angryEndAni.push_back(sf::IntRect(455, 1025, 431, 505));
	angryEndAni.push_back(sf::IntRect(455, 511, 437, 512));
	angryEndAni.push_back(sf::IntRect(894, 538, 322, 470));
	angryEndAni.push_back(sf::IntRect(1218, 538, 152, 363));
	angryEndAni.push_back(sf::IntRect(1280, 903, 112, 242));
	angryEndAnimation.setAnimationRects(angryEndAni);
	angryEndAnimation.setFrameRate(20);
	angryEndAnimation.setLoopEnabled(false);
	std::vector<sf::IntRect> angryAni;
	angryAni.push_back(sf::IntRect(0, 537, 393, 575));
	angryAni.push_back(sf::IntRect(0, 0, 393, 535));
	angryAni.push_back(sf::IntRect(790, 0, 393, 542));
	angryAni.push_back(sf::IntRect(1185, 558, 393, 545));
	angryAni.push_back(sf::IntRect(1185, 0, 393, 556));
	angryAni.push_back(sf::IntRect(790, 1136, 393, 582));
	angryAni.push_back(sf::IntRect(790, 544, 393, 590));
	angryAni.push_back(sf::IntRect(1185, 1105, 393, 595));
	angryAni.push_back(sf::IntRect(0, 1114, 393, 615));
	angryAni.push_back(sf::IntRect(395, 638, 393, 621));
	angryAni.push_back(sf::IntRect(395, 0, 393, 636));
	angryAnimation.setAnimationRects(angryAni);
	angryAnimation.setFrameRate(20);
	//mySprite.setPosition(1600, 700);
	if (textFont.loadFromFile("../assets/DJB Speak the Truth.ttf"))
	{
		objText.setFont(textFont);
	}
	objText.setColor(sf::Color(20, 20, 40));
	//TODO: text position
	/*objText.setPosition(850, 1030);*///change to dynamic positioning later
	infoBuffer.loadFromFile("../assets/Sound Files/babbel_Info_ver1.wav");
	infoShortBuffer.loadFromFile("../assets/Sound Files/babbel_InfoShort_ver1.wav");
	angryBuffer.loadFromFile("../assets/Sound Files/babbel_Angry_ver1.wav");
	angryShortBuffer.loadFromFile("../assets/Sound Files/babbel_AngryShort_ver1.wav");
	infoSFX.setBuffer(infoBuffer);
	infoShortSFX.setBuffer(infoShortBuffer);
	angrySFX.setBuffer(angryBuffer);
	angryShortSFX.setBuffer(angryShortBuffer);
	scale.x = 1;
	scale.y = 1;
	isTalking = false;
	talkTime = 5.f;
	speachBubbleTexture.loadFromFile("../assets/Sprites/pager_pratbubbla.png");
	speachBubble.setTexture(speachBubbleTexture);
}

Boss::~Boss()
{
}

void Boss::Update(float deltaTime)
{
	if (bossTalk == true)
	{
		if (isTalking == false)
		{
			switch (myType)
			{
			case INFORMATIVE:
				/*objText.setString("People are moving out! Do your job!");*/
				infoSFX.play();
				isTalking = true;
				break;
			case ANGRY:
				//objText.setString("dfghshgartera");//Words
				angrySFX.play();
				isTalking = true;
				break;
			case TUTORIAL1:
				//objText.setString("Move your cartank with the A and D keys");//Words
				infoSFX.play();
				isTalking = true;
				break;
			case TUTORIAL2:
				//objText.setString("Aim with your mouse and shoot with the left mouse button");//Words
				infoSFX.play();
				isTalking = true;
				break;
			case TUTORIAL3:
				//objText.setString("Set turrets with the space key, turrets cost 500 market value");//Words
				infoSFX.play();
				isTalking = true;
				break;
			case TUTORIAL4:
				//objText.setString("Use power ups with the right mouse button");//Words
				infoSFX.play();
				isTalking = true;
				break;
			case ANGRYSHORT:
				angryShortSFX.play();
				isTalking = true;
				break;
			case INFORMATIVESHORT:
				infoShortSFX.play();
				isTalking = true;
				break;
			}
		}
		if (myClock.getElapsedTime().asSeconds() > talkTime)
		{
			bossTalk = false;
			myClock.restart();
			isTalking = false;
		}
		//update animation here
		angryAnimation.updateAnimation(deltaTime);
		angryEndAnimation.updateAnimation(deltaTime);
		angryStartAnimation.updateAnimation(deltaTime);
		normalAnimation.updateAnimation(deltaTime);
		happyAnimation.updateAnimation(deltaTime);
	}
	
}

void Boss::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	if (bossTalk == true)
	{
		//Change active animation here with clock
		switch(myType)
		{
		case ANGRY://make ifs doesn't work
			target.draw(angryAnimation, states);
			
			break;
		case ANGRYSHORT:
			target.draw(angryAnimation, states);
			break;
		case INFORMATIVE:
			target.draw(normalAnimation, states);
			break;
		case INFORMATIVESHORT:
			target.draw(happyAnimation, states);
			break;
		case TUTORIAL1:
			target.draw(normalAnimation, states);
			break;
		case TUTORIAL2:
			target.draw(happyAnimation, states);
			break;
		case TUTORIAL3:
			target.draw(normalAnimation, states);
			break;
		case TUTORIAL4: 
			target.draw(happyAnimation, states);
			break;
		}
		//target.draw(speachBubble, states);
		target.draw(objText, states);
	}
}

void Boss::RelativePosition(sf::View & acqCamera)
{
	/*mySprite.setPosition(acqCamera.getCenter().x + 20, acqCamera.getCenter().y + 20);
	mySprite.setRotation(acqCamera.getRotation());*/
}

void Boss::BossTalk(BOSSSTATE acqState, const sf::String(&acqText), float acqTalkTime)
{
	bossTalk = true;
	if (acqState != TUTORIAL1)
	{
		if (acqState != TUTORIAL2)
		{
			if (acqState != TUTORIAL3)
			{
				myClock.restart();
			}
		}
	}
	myType = acqState;
	objText.setString(acqText);
	talkTime = acqTalkTime;
	aniClock.restart();
	/*if (isTalking == false)
	{
		switch (myType)
		{
		case INFORMATIVE:
			angrySFX.play();
			break;
		case ANGRY:
			angryShortSFX.play();
			break;
		case TUTORIAL1:
			infoSFX.play();
			break;
		case TUTORIAL2:
			infoSFX.play();
			break;
		case TUTORIAL3:
			infoSFX.play();
			break;
		case TUTORIAL4:
			infoSFX.play();
			break;
		}
	}*/
}

void Boss::GetWindowSize(sf::Vector2u(acqWindow))
{
	windowSize.x = acqWindow.x;
	windowSize.y = acqWindow.y;
	scale.x = windowSize.x / 1920.f;
	scale.y = windowSize.y / 1080.f;
	angryAnimation.setScale(-scale.x, scale.y);
	angryAnimation.setPosition(windowSize.x * 0.19, windowSize.y * 0.5);
	normalAnimation.setScale(-scale.x, scale.y);
	normalAnimation.setPosition(windowSize.x * 0.19, windowSize.y * 0.5);
	happyAnimation.setScale(-scale.x, scale.y);
	happyAnimation.setPosition(windowSize.x * 0.19, windowSize.y * 0.5);
	angryStartAnimation.setScale(-scale.x, scale.y);
	angryStartAnimation.setPosition(windowSize.x * 0.19, windowSize.y * 0.5);
	angryEndAnimation.setScale(-scale.x, scale.y);
	angryEndAnimation.setPosition(windowSize.x * 0.19, windowSize.y * 0.5);
	speachBubble.setScale(scale.x * 1.5f, scale.y);
	speachBubble.setPosition(windowSize.x * 0.16f, windowSize.y * 0.72f);
	objText.setScale(scale);
	objText.setPosition(windowSize.x * 0.18f, windowSize.y * 0.75);
}

BOSSSTATE Boss::GetState()
{
	return BOSSSTATE(myType);
}
