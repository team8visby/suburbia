#pragma once
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>

class Cloud : public sf::Drawable, public sf::Transformable
{
public:
	Cloud();
	~Cloud();
	void update(float dt);
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
private:
	float radiusPos;
	float anglePos;
	sf::Texture playerTexture;
	sf::Sprite cannonSprite;
};
