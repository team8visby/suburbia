#include "stdafx.h"
#include "MenuState.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "Gamestate.h"
#include "HighScoreState.h"


MenuState::MenuState(sf::RenderWindow* p_window)
{
	window = p_window;

	window->setMouseCursorVisible(true);
	if (!menuTexture.loadFromFile("../assets/Sprites/Menu.png"))
	{
		sf::err();
	}
	menuMusic.openFromFile("../assets/Sound Files/doodle.wav");
	menuMusic.setLoop(true);
	menuMusic.play();
	buffer.loadFromFile("../assets/Sound Files/scribble_ver1.wav");
	scribbleSFX.setBuffer(buffer);
	scribblePlayed = false;
	startButton = new MenuButton("Start Game", &menuTexture, 
		sf::Vector2f(window->getSize().x * 0.078, window->getSize().y * 0.35), 
		sf::Vector2f(window->getSize().x * 0.4, window->getSize().y * 0.1));
	scoreButton = new MenuButton("HighScores", &menuTexture,
		sf::Vector2f(window->getSize().x * 0.078, window->getSize().y * 0.46),
		sf::Vector2f(window->getSize().x * 0.4, window->getSize().y * 0.1));
	/*
	optionsButton = new MenuButton("Options", &menuTexture,
		sf::Vector2f(window->getSize().x * 0.08, window->getSize().y * 0.57),
		sf::Vector2f(window->getSize().x * 0.4, window->getSize().y * 0.1));
	*/
	quitButton = new MenuButton("Quit Game", &menuTexture,
		sf::Vector2f(window->getSize().x * 0.078, window->getSize().y * 0.675),
		sf::Vector2f(window->getSize().x * 0.4, window->getSize().y * 0.1));
	mainMenuSprite.setTexture(menuTexture);
	mainMenuSprite.setTextureRect(sf::IntRect(0, 0, 1920, 1080));
	auto rect = mainMenuSprite.getGlobalBounds();
	mainMenuSprite.setOrigin(rect.width / 2, rect.height / 2);
	mainMenuSprite.setPosition(window->getSize().x / 2, window->getSize().y / 2);
	float scale = std::min(window->getSize().x / rect.width,
		window->getSize().y / rect.height);
	mainMenuSprite.setScale(scale, scale);
	menuBockSprite.setTexture(menuTexture);
	menuBockSprite.setTextureRect(sf::IntRect(0, 2278, 101, 95));
	menuBockSprite.setOrigin(62 / 2, 98 / 2);
	menuBockSprite.setScale(scale, scale);

	planet.setScale(scale * 1.44, scale * 1.44);
	planet.setPosition(window->getSize().x * 0.5, window->getSize().y * 2.6);
	planet.setBordersEnabled(false);
	sky.setScale(scale * 1.25, scale * 1.25);
	sky.setPosition(window->getSize().x * 0.5, window->getSize().y * 2.6);

	selected = BUTTON_NONE;
}

MenuState::~MenuState()
{
	delete startButton;
	delete scoreButton;
//	delete optionsButton;
	delete quitButton;
}

void MenuState::Exit()
{

}

bool MenuState::Update(float dt)
{
	if (quitButton->hover(sf::Mouse::getPosition(*window)))
	{
		selected = BUTTON_QUIT;
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			window->close();
		}
		if (scribbleSFX.getStatus() == scribbleSFX.Stopped && scribblePlayed == false)
		{
			scribbleSFX.play();
			scribblePlayed = true;
		}
	}
	if (startButton->hover(sf::Mouse::getPosition(*window)))
	{
		selected = BUTTON_START;
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			return false;
		}
		if (scribbleSFX.getStatus() == scribbleSFX.Stopped && scribblePlayed == false)
		{
			scribbleSFX.play();
			scribblePlayed = true;
		}
	
	}
	if (scoreButton->hover(sf::Mouse::getPosition(*window)))
	{
		selected = BUTTON_HIGHSCORE;
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			return false;
		}
		if (scribbleSFX.getStatus() == scribbleSFX.Stopped && scribblePlayed == false)
		{
			scribbleSFX.play();
			scribblePlayed = true;
		}
	}
	if (!startButton->hover(sf::Mouse::getPosition(*window)) &&
		!quitButton->hover(sf::Mouse::getPosition(*window)) &&
//		!optionsButton->hover(sf::Mouse::getPosition(*window)) &&
		!scoreButton->hover(sf::Mouse::getPosition(*window)))
	{
		selected = BUTTON_NONE;
		scribblePlayed = false;
	}
	/* if (optionsButton->hover(sf::Mouse::getPosition(*window)))
	{
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			return false;
		}
	} */
	sky.Update(dt);
	planet.rotate(-.1 * dt);
	return true;
}

void MenuState::Enter()
{

}

void MenuState::Draw(sf::RenderWindow* window)
{
	window->draw(sky);
	window->draw(planet);
	window->draw(mainMenuSprite);
	window->draw(*startButton);
	window->draw(*scoreButton);
//	window->draw(*optionsButton);
	window->draw(*quitButton);
}

IState* MenuState::NextState()
{
	switch (selected)
	{
	case BUTTON_START:
		return new Gamestate(window);
	case BUTTON_HIGHSCORE:
		return new HighScoreState(0, window);
	default:
		return nullptr;
	}
}
