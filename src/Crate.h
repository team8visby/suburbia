#pragma once
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include "AnimatedSprite.h"

class Crate : public sf::Drawable, public sf::Transformable
{
public:
	Crate(sf::Texture* crateTexture, int powerupType, float anglePos, float radiusPos);
	~Crate();
	bool isOpened();
	bool isDestroyed();
	int getPowerupType();
	float getAnglePos();
	float getRadiusPos();
	void destroy();
	void update(float dt);
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
private:
	float despawnTimer;
	float anglePos;
	float radiusPos;
	bool destroyed;
	bool opened;
	int powerupType;
	AnimatedSprite landAnimation;
	AnimatedSprite fallAnimation;
};
