#include "FreezeRay.h"

FreezeRay::FreezeRay(sf::Vector2f pos, float rotation, EnemyManager* enemymanager) : Projectile(pos, rotation)
{
	/*raysize.x = 5.f;
	raysize.y = 500.f;
	rayform;(pos,raysize);*/
	rayShape.setPosition(pos);
	rayShape.setRadius(5);
	rayShape.setFillColor(sf::Color(0, 0, 0xFF, 126));

}

PROJECTILETYPE FreezeRay::GetProjectileType()
{
	return PROJ_RAY;
}

void FreezeRay::Update(float dt)
{
	Projectile::Update(dt);
}

void FreezeRay::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(rayShape, states);
}

sf::CircleShape FreezeRay::getCollider()
{
	return rayShape;
}