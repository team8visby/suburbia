#pragma once
#include "Projectile.h"

class Missile : public Projectile
{
public:
	Missile(sf::Vector2f pos, float rotation, EnemyManager* enemyManager, sf::Texture &projtext);
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	void Update(float dt) override;
	int getStrenght() override;
	PROJECTILETYPE GetProjectileType() override;
private:
	sf::Sprite projectileSprite;
	sf::Texture projectileTexture;
	float homingDelayTimer;
	EnemyManager* enemyManager;
};