#pragma once
#include "Istate.h"
#include "AudioManager.h"
#include "Planet.h"
#include "Sky.h"

class HighScoreState : public IState
{
public:
	HighScoreState(int score, sf::RenderWindow* window);
	~HighScoreState();
	void Enter();
	void Exit();
	bool Update(float dt);
	void Draw(sf::RenderWindow* window);
	IState* NextState();
	void event(sf::Event keyEvent);
private:
	void updateScores();
	void addScore(int score, std::string name);
	void saveScores();
	void loadScores();
	bool HighScoreState::isANewHighScore(int score);

	bool enteringScore;
	float finalScore, finalTime, scale; //Score imported from the game or menu state, menu size
	sf::RenderWindow* window;
	sf::Texture background;
	sf::Sprite back;
	sf::Font scoreFont;
	sf::Text nameInput;
	sf::Text titleText;
	sf::Text newHighScoreText;
	sf::Text pleaseEnterNameText;
	AudioManager ptrAudioManager;
	struct ScoreEntry { std::string name; int score; };
	std::vector<ScoreEntry> scores;
	sf::Text numbers[10];
	sf::Text nametext[10];
	sf::Text scoretext[10];
	std::string nameEntry;
	Planet planet;
	Sky sky;
};

