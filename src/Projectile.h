#pragma once
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/CircleShape.hpp>

#include "ProjectileManager.h"


class Projectile : public sf::Drawable, public sf::Transformable
{
public:
	Projectile(sf::Vector2f pos, float rotation, sf::Texture &projtext);
	~Projectile();
	virtual void Update(float dt);
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	virtual int getStrenght();
	virtual sf::CircleShape getCollider();
	void Destroy();
	bool isDestroyed() const;
	virtual PROJECTILETYPE GetProjectileType();
private:
	sf::Sprite projectileSprite;
	float lifeTimeTimer;
	bool destroyed;

	int streght;



};
