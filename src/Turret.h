#pragma once
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/System/Clock.hpp>

class EnemyShip;
class ProjectileManager;

class Turret : public sf::Drawable, public sf::Transformable
{
public:
	Turret(sf::Vector2f pos, float rotation, ProjectileManager* projManager);
	~Turret();
	void Update(float dt);
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	float getRange();
	sf::CircleShape getCollider();
	void setTarget(EnemyShip* target);
	void Fire();
private:
	float range;
	float fireRateTimer;
	float spawnTimer;
	ProjectileManager* projectileManager;
	sf::Transformable* target;
	sf::Texture turretTexture;
	sf::Sprite feetSprite;
	sf::Sprite cannonSprite;
	sf::Sprite cannonPipeSprite;
};
