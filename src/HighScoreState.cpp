#include "HighScoreState.h"
#include "MenuState.h"

#include <json.hpp>
#include <fstream>

HighScoreState::HighScoreState(int score, sf::RenderWindow* p_window)
{
	enteringScore = false;
	window = p_window;
	finalScore = score;
	finalTime = 1;
	background.loadFromFile("../assets/Sprites/Menu.png");
	back.setTexture(background);
	back.setTextureRect(sf::IntRect(1921, 0, 1920, 1080));
	scale = std::min(window->getSize().x / back.getGlobalBounds().width, window->getSize().y / back.getGlobalBounds().height);
	back.setScale(scale, scale);
	planet.setScale(scale * 1.44, scale * 1.44);
	planet.setPosition(window->getSize().x * 0.5, window->getSize().y * 2.6);
	planet.setBordersEnabled(false);
	sky.setScale(scale * 1.25, scale * 1.25);
	sky.setPosition(window->getSize().x * 0.5, window->getSize().y * 2.6);
}

HighScoreState::~HighScoreState()
{
}

void HighScoreState::Enter()
{
	loadScores();
	updateScores();
	if (scoreFont.loadFromFile("../assets/{skinny} jeans solid.ttf"))
	{
		auto windowSize = window->getSize();
		float scaleMult = 1.3;
		for (int i = 0; i < 10; i++)
		{
			numbers[i].setString(std::to_string(i + 1));
			numbers[i].setFont(scoreFont);
			numbers[i].setColor(sf::Color(0, 0, 0));
			numbers[i].setPosition(windowSize.x * 0.1, windowSize.y * (0.35 + i * 0.054));
			numbers[i].setScale(scale * scaleMult, scale * scaleMult);
			nametext[i].setFont(scoreFont);
			nametext[i].setColor(sf::Color(0,0,0));
			nametext[i].setPosition(windowSize.x * 0.15, windowSize.y * (0.35 + i * 0.054));
			nametext[i].setScale(scale * scaleMult, scale * scaleMult);
			scoretext[i].setFont(scoreFont);
			scoretext[i].setColor(sf::Color(0, 0, 0));
			scoretext[i].setPosition(windowSize.x * 0.39, windowSize.y * (0.35 + i * 0.054));
			scoretext[i].setScale(scale * scaleMult, scale * scaleMult);
		}
		nameInput.setFont(scoreFont);
		nameInput.setColor(sf::Color(0, 0, 0));
		nameInput.setScale(scale * scaleMult * 1.1, scale * scaleMult * 1.1);
		nameInput.setPosition(windowSize.x * 0.15, windowSize.y * 0.29);

		titleText.setFont(scoreFont);
		titleText.setColor(sf::Color(0, 0, 0));
		titleText.setString("Top Agents");
		titleText.setScale(scale * 2, scale * 2);
		titleText.setPosition(windowSize.x * 0.15, windowSize.y * 0.13);

		newHighScoreText.setFont(scoreFont);
		newHighScoreText.setColor(sf::Color(0, 0, 0));
		newHighScoreText.setString("New Highscore!");
		newHighScoreText.setScale(scale * scaleMult, scale * scaleMult);
		newHighScoreText.setPosition(windowSize.x * 0.15, windowSize.y * 0.2);

		pleaseEnterNameText.setFont(scoreFont);
		pleaseEnterNameText.setColor(sf::Color(0, 0, 0));
		pleaseEnterNameText.setString("Please enter your name:");
		pleaseEnterNameText.setScale(scale * scaleMult * 0.9, scale * scaleMult * 0.9);
		pleaseEnterNameText.setPosition(windowSize.x * 0.15, windowSize.y * 0.24);
	}
	enteringScore = isANewHighScore(finalScore);
}

void HighScoreState::Exit()
{
}

bool HighScoreState::Update(float dt)
{
	sky.Update(dt);
	planet.rotate(-.1 * dt);
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
	{
		return false;
		//Exit, Set state to Menu
	}
	return true;
}

void HighScoreState::Draw(sf::RenderWindow* window)
{
	window->draw(sky);
	window->draw(planet);
	window->draw(back);
	for (int i = 0; i < 10; i++)
	{
		window ->draw(numbers[i]);
		window->draw(nametext[i]);
		window->draw(scoretext[i]);
	}
	window->draw(nameInput);
	window->draw(titleText);
	if (enteringScore)
	{
		window->draw(newHighScoreText);
		window->draw(pleaseEnterNameText);
	}
}

IState* HighScoreState::NextState()
{
	return new MenuState(window);
}

void HighScoreState::event(sf::Event event)
{
	if (enteringScore)
	{
		switch (event.type)
		{
		case sf::Event::KeyPressed:
			if (event.key.code == sf::Keyboard::Return)
			{
				nameInput.setString("");
				addScore(finalScore, nameEntry);
				updateScores();
				saveScores();
				enteringScore = false;
			}
			break;

		case sf::Event::TextEntered:
			if (event.text.unicode == '\b')
			{
				if (nameEntry.size() != 0)
				{
					nameEntry.erase(nameEntry.size() - 1);
					nameInput.setString(nameEntry);
				}
			}
			else if (nameEntry.size() <= 18 && event.text.unicode < 128)
			{
				nameEntry += static_cast<char>(event.text.unicode);
				nameInput.setString(nameEntry);
			}
			break;
		default:
			break;
		}
	}
}

void HighScoreState::updateScores()
{
	int count = 0;
	auto iter = scores.begin();
	while (iter != scores.end())
	{
		if (count >= 10)
		{
			break;
		}
		nametext[count].setString(scores[count].name);
		scoretext[count].setString(std::to_string(scores[count].score));
		count++;
		++iter;
	}
}

void HighScoreState::addScore(int score, std::string name)
{
	auto iter = scores.begin();
	while (iter != scores.end())
	{
		if (finalScore > (*iter).score)
		{
			ScoreEntry entry;
			entry.score = score;
			entry.name = name;
			scores.insert(iter, entry);
			break;
		}
		iter++;
	}
}

bool HighScoreState::isANewHighScore(int score)
{
	auto iter = scores.begin();
	while (iter != scores.end())
	{
		if (score > (*iter).score)
		{
			printf("New highscore!");
			return true;
		}
		iter++;
	}
	return false;
}

void HighScoreState::saveScores()
{
	nlohmann::json scoreJson;
	scoreJson["scores"].array();
	for (int i = 0; i < 10; i++)
	{
		scoreJson["scores"][i]["name"] = scores[i].name;
		scoreJson["scores"][i]["score"] = scores[i].score;
	}
	std::ofstream scoreFile;
	scoreFile.open("../assets/Score.json");
	scoreFile.clear();
	scoreFile << scoreJson.dump();
	scoreFile.close();
}

void HighScoreState::loadScores()
{
	scores.clear();
	std::ifstream scoreFile;
	scoreFile.open("../assets/Score.json");
	nlohmann::json scorejson;
	if (scoreFile.is_open())
	{
		while (!scoreFile.eof())
		{
			try
			{
				scoreFile >> scorejson;
			}
			catch (std::exception& e)
			{
				printf("Exception: %s\n", e.what());
			}
		}
		scoreFile.close();
	}

	for (auto it = scorejson["scores"].begin(); it != scorejson["scores"].end(); ++it)
	{
		try
		{
			ScoreEntry entry;
			entry.name = (*it)["name"].get<std::string>();
			entry.score = (*it)["score"].get<int>();
			scores.push_back(entry);
		}
		catch (std::exception& e)
		{
			printf("Exception: %s\n", e.what());
		}
	}
}
