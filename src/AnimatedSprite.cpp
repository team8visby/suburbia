#include "AnimatedSprite.h"

AnimatedSprite::AnimatedSprite()
{
	frameRate = 1;
	currentFrame = 0;
	isLooping = true;
	lifeTimeClock = 0;
}

void AnimatedSprite::updateAnimation(float dt)
{
	lifeTimeClock += dt;
	if (animationFrames.size() >= 1)
	{
		currentFrame += dt * frameRate;
		while (currentFrame < 0 && isLooping)
		{
			currentFrame += animationFrames.size() - 1;
		}
		while (currentFrame > animationFrames.size() - 1 && isLooping)
		{
			currentFrame -= animationFrames.size() - 1;
		}
		if (animationFrames.size() > currentFrame && currentFrame >= 0)
		{
			setTextureRect(animationFrames[static_cast<int>(currentFrame)]);
		}
	}
}

void AnimatedSprite::setFrameRate(float framesPerSecond)
{
	frameRate = framesPerSecond;
}

void AnimatedSprite::setAnimationRects(std::vector<sf::IntRect> animationRects)
{
	setTextureRect(animationRects[0]);
	animationFrames = animationRects;
}

void AnimatedSprite::restartAnimation()
{
	currentFrame = 0;
	updateAnimation(0);
}

float AnimatedSprite::getLifetime()
{
	return lifeTimeClock;
}

void AnimatedSprite::setLoopEnabled(bool enabled)
{
	isLooping = enabled;
}

bool AnimatedSprite::isPlaybackFinished()
{
	if (!isLooping && currentFrame > animationFrames.size())
	{
		return true;
	}
	return false;
}
