#include "stdafx.h"
#include "BuildingManager.h"
#include "Planet.h"
#include "Building.h"
#include "EnemyShip.h"
#include "Boss.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "CollisionManager.h"

BuildingManager::BuildingManager( Planet * getPlanet)
{
	ptrPlanet = getPlanet;
	buildingTexture1.loadFromFile("../assets/Sprites/1 Hus sprite.png");
	buildingTexture2.loadFromFile("../assets/Sprites/2 Hus sprite.png");
	destroyedBuildings = 0;
}

BuildingManager::~BuildingManager()
{
	auto it = vecPtrBuildings.begin();
	while (it != vecPtrBuildings.end())
	{
		delete (*it);
		(*it) = nullptr;
		++it;
	}
	vecPtrBuildings.clear();
}

void BuildingManager::CheckBuilding(EnemyShip * getEnemyShip)
{
	auto it = vecPtrBuildings.begin();
	while (it != vecPtrBuildings.end())
	{
		if ((*it)->getDamageType() == NO_DAMAGE)
		{
			if (CollisionManager::CheckCircles(getEnemyShip->getCollider(), (*it)->GetCollider()))
			{

				(*it)->SetDamageType(DAMAGED);
				++destroyedBuildings;
			}
		}
		it++;
	}
}

void BuildingManager::Update(float deltaTime)
{
	auto it = vecPtrBuildings.begin();
	while (it != vecPtrBuildings.end())
	{
		(*it)->Update(deltaTime);
		++it;
	}
}


void BuildingManager::Draw(sf::RenderWindow * window)
{
	auto it = vecPtrBuildings.begin();
	while (it != vecPtrBuildings.end())
	{
		window->draw(*(*it));
		++it;
	}

	
}

void BuildingManager::CreateBuildings()
{
	float offset = 8;
	for (int i = 0; i < 30; i++)
	{
		if (i < 3)
		{
			vecPtrBuildings.push_back(new Building(buildingTexture1, TYPE1, sf::Vector2f(cosf(i)* (ptrPlanet->getRadiusSize() + offset), sinf(i)* (ptrPlanet->getRadiusSize() + offset)), i * 180 / M_PI));
		}
		else if (i < 6)
		{
			vecPtrBuildings.push_back(new Building(buildingTexture1, TYPE2, sf::Vector2f(cosf(i)* (ptrPlanet->getRadiusSize() + offset), sinf(i)* (ptrPlanet->getRadiusSize() + offset)), i * 180 / M_PI));
		}
		else if (i < 9)
		{
			vecPtrBuildings.push_back(new Building(buildingTexture1, TYPE3, sf::Vector2f(cosf(i)* (ptrPlanet->getRadiusSize() + offset), sinf(i)* (ptrPlanet->getRadiusSize() + offset)), i * 180 / M_PI));
		}
		else if (i < 12)
		{
			vecPtrBuildings.push_back(new Building(buildingTexture1, TYPE4, sf::Vector2f(cosf(i)* (ptrPlanet->getRadiusSize() + offset), sinf(i)* (ptrPlanet->getRadiusSize() + offset)), i * 180 / M_PI));
		}
		else if (i < 15)
		{
			vecPtrBuildings.push_back(new Building(buildingTexture1, TYPE5, sf::Vector2f(cosf(i)* (ptrPlanet->getRadiusSize() + offset), sinf(i)* (ptrPlanet->getRadiusSize() + offset)), i * 180 / M_PI));
		}
		else if (i < 18)
		{
			vecPtrBuildings.push_back(new Building(buildingTexture2, TYPE6, sf::Vector2f(cosf(i)* (ptrPlanet->getRadiusSize() + offset), sinf(i)* (ptrPlanet->getRadiusSize() + offset)), i * 180 / M_PI));
		}
		else if (i < 21)
		{
			vecPtrBuildings.push_back(new Building(buildingTexture2, TYPE7, sf::Vector2f(cosf(i)* (ptrPlanet->getRadiusSize() + offset), sinf(i)* (ptrPlanet->getRadiusSize() + offset)), i * 180 / M_PI));
		}
		else if (i < 24)
		{
			vecPtrBuildings.push_back(new Building(buildingTexture2, TYPE8, sf::Vector2f(cosf(i)* (ptrPlanet->getRadiusSize() + offset), sinf(i)* (ptrPlanet->getRadiusSize() + offset)), i * 180 / M_PI));
		}
		else if (i < 27)
		{
			vecPtrBuildings.push_back(new Building(buildingTexture2, TYPE9, sf::Vector2f(cosf(i)* (ptrPlanet->getRadiusSize() + offset), sinf(i)* (ptrPlanet->getRadiusSize() + offset)), i * 180 / M_PI));
		}
		else if (i < 30)
		{
			vecPtrBuildings.push_back(new Building(buildingTexture2, TYPE10, sf::Vector2f(cosf(i)* (ptrPlanet->getRadiusSize() + offset), sinf(i)* (ptrPlanet->getRadiusSize() + offset)), i * 180 / M_PI));
		}
	}
}

void BuildingManager::GetCameraPos(sf::View &acqCamera)//call in gamestate update
{
	//put camera into the boss
	
}

int BuildingManager::GetDestroydBuildings()
{
	return destroyedBuildings;
}
