#include "stdafx.h"
#include "PowerUpManager.h"

class PowerUp : public sf::Transformable, public sf::Drawable
{
	sf::Sprite ptrSprite;
	int myType;
	float mySpeed;
	float myRadius;
	float relativeAngle;
	float relativeRadius;
	float timeoutClock;
	int transparent;
	bool destroyed;
	bool landed;
public:
	PowerUp(sf::Texture * powerUpTexture, int getType, float getRelativeDegree, float getRelativeRadius);
	~PowerUp();
	void Update(float dt);
	void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	void destroy();
	bool isDestroyed();
	float GetRadius();
	sf::CircleShape getCollider();
	POWERTYPE GetType();
};