#pragma once
#include <SFML/Graphics/Sprite.hpp>
#include <vector>

class AnimatedSprite : public sf::Sprite
{
public:
	AnimatedSprite();
	void updateAnimation(float dt);
	void setFrameRate(float framesPerSecond);
	void setAnimationRects(std::vector<sf::IntRect> animationRects);
	void restartAnimation();
	float getLifetime();
	void setLoopEnabled(bool enabled);
	bool isPlaybackFinished();
private:
	float frameRate;
	float currentFrame;
	float lifeTimeClock;
	bool isLooping;
	std::vector<sf::IntRect> animationFrames;
};
