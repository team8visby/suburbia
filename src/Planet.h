#pragma once
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

class Planet : public sf::Transformable, public sf::Drawable
{
public:
	Planet();
	~Planet();
	void Update(float dt);
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	void setBorders(float cwBorderAngle, float ccwBorderAngle);
	void setBordersEnabled(bool enabled);
	void setPosition(float x, float y);
	void setPosition(sf::Vector2f pos);
	void setScale(sf::Vector2f scale);
	void setScale(float x, float y);
	void rotate(float angle);
	void setRotation(float angle);
	float getRadiusSize();
	float getCwBorder();
	float getCcwBorder();
	bool isBordersEnabled();
private:
	sf::Texture planetTexture;
	sf::Sprite planetShape1;
	sf::Sprite planetShape2;
	sf::Sprite planetShape3;
	sf::Sprite planetShape4;
	bool bordersEnabled;
	float skyScale;
	sf::RectangleShape ccwBorder;
	sf::RectangleShape cwBorder;
};
