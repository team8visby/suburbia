#pragma once
#include "stdafx.h"

enum DAMAGESTATE;
enum BUILDINGTYPE;


class Building : public sf::Drawable, public sf::Transformable
{
	DAMAGESTATE damageType;
	int buildingType;
	sf::Sprite mySprite;
	sf::Texture myTexture;
public:
	Building(sf::Texture &acqTexture, BUILDINGTYPE getType, sf::Vector2f getPosition, float getRotation);
	~Building();
	void Update(float deltaTime);
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	void SetDamageType(DAMAGESTATE getState);
	DAMAGESTATE getDamageType();
	sf::CircleShape GetCollider();
};
