#pragma once
#include "stdafx.h"
class MenuButton : public sf::Drawable
{
public:
	MenuButton();
	MenuButton(char* bText, sf::Texture *menuTexture , sf::Vector2f pos, sf::Vector2f size);
	bool hover(sf::Vector2i mousePos);
	void draw(sf::RenderTarget &target, sf::RenderStates states) const override;
	~MenuButton();

private:
	bool init;
	bool hovered;
	sf::Sprite boxSprite;
	sf::Sprite checkSprite;
	sf::Text buttonText;
	sf::FloatRect collisionRect;
	sf::Font buttonFont;
};

