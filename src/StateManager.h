#pragma once
#include "stdafx.h"
class StateManager;
class IState;
class StateManager {
public:
	StateManager(sf::RenderWindow* window);
	~StateManager();
	bool Update();
	void Draw(sf::RenderWindow* window);
	void event(sf::Event keyEvent);
private:
	void SetState(IState* p_xState);
	IState* m_pxCurrentState;
	sf::Time m_xLastTick;
	sf::Clock m_xClock;
};