#pragma once
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <vector>
#include <SFML/Graphics/Sprite.hpp>
#include "FreezeShot.h"

class MiniMap : public sf::Transformable, public sf::Drawable
{
public:
	MiniMap();
	~MiniMap() {};
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	void update(float dt);
	void setRotation(float angle);
	void setPosition(sf::Vector2f pos);
	void setPosition(float x, float y);
	void addDot(sf::Vector2f pos, sf::Color color);
	void setScale(sf::Vector2f scale);
	void setScale(float x, float y);
	void notify(sf::Vector2f pos, sf::Color color);
	void setSectorColor(int sector, sf::Color color);
private:
	sf::Texture mapTexture;
	std::vector<sf::Sprite> radarDots;
	std::vector<sf::Sprite> radarAlerts;
	AnimatedSprite holo;
	sf::Sprite sector1;
	sf::Sprite sector2;
	sf::Sprite sector3;
	sf::Sprite sector4;
};
