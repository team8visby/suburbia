#include "MiniMap.h"

MiniMap::MiniMap()
{
	mapTexture.loadFromFile("../assets/Map.png");
	sector1.setTexture(mapTexture);
	sector1.setTextureRect(sf::IntRect(501, 331, 184, 133));
	sector1.setOrigin(sector1.getLocalBounds().width / 2, sector1.getLocalBounds().height - 1);
	sector2 = sf::Sprite(sector1);
	sector2.rotate(90);
	sector3 = sf::Sprite(sector2);
	sector3.rotate(90);
	sector4 = sf::Sprite(sector3);
	sector4.rotate(90);

	std::vector<sf::IntRect> aniFrames;
	aniFrames.push_back(sf::IntRect(501, 0, 499, 329));
	aniFrames.push_back(sf::IntRect(0, 331, 499, 329));
	aniFrames.push_back(sf::IntRect(0, 0, 499, 329));
	aniFrames.push_back(sf::IntRect(0, 662, 499, 329));
	holo.setTexture(mapTexture);
	holo.setAnimationRects(aniFrames);
	holo.setLoopEnabled(true);
	holo.setFrameRate(10);
	holo.setScale(1.5, 1.5);
	holo.setOrigin(holo.getLocalBounds().width / 2, holo.getLocalBounds().height / 2);
}

void MiniMap::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(sector1, states);
	target.draw(sector2, states);
	target.draw(sector3, states);
	target.draw(sector4, states);
	{
		auto it = radarDots.begin();
		while (it != radarDots.end())
		{
			target.draw((*it), states);
			++it;
		}
	}
	{
		auto it = radarAlerts.begin();
		while (it != radarAlerts.end())
		{
			target.draw((*it), states);
			++it;
		}
	}
	target.draw(holo, states);
}

void MiniMap::update(float dt)
{
	radarDots.clear();
	holo.updateAnimation(dt);

	auto it = radarAlerts.begin();
	while (it != radarAlerts.end())
	{
		if ((*it).getScale().x >= getScale().x * 3)
		{
			sf::Color col = (*it).getColor();
			col.a = std::max(col.a - 1500 * dt, 0.f);
			(*it).setColor(col);

			if ((*it).getScale().x >= getScale().x * 6)
			{
				it = radarAlerts.erase(it);
				continue;
			}
		}
		(*it).scale(1 + 3 * dt, 1 + 3 * dt);
		++it;
	}
}

void MiniMap::setRotation(float angle)
{
	angle += 45;
	float oldAngle = getRotation();
	sf::Transformable::setRotation(angle);
	sector1.setRotation(angle);
	sector2.setRotation(angle + 90);
	sector3.setRotation(angle + 180);
	sector4.setRotation(angle + 270);

	sf::Transform transf;
	transf.rotate(angle - oldAngle);

	auto it = radarAlerts.begin();
	while (it != radarAlerts.end())
	{
		sf::Vector2f pos = (*it).getPosition();
		pos.x -= getPosition().x;
		pos.y -= getPosition().y;
		pos = transf.transformPoint(pos);
		pos.x += getPosition().x;
		pos.y += getPosition().y;
		(*it).setPosition(pos);
		++it;
	}
}

void MiniMap::setPosition(sf::Vector2f pos)
{
	setPosition(pos.x, pos.y);
}

void MiniMap::setPosition(float x, float y)
{
	sf::Transformable::setPosition(x, y);
	sector1.setPosition(x, y);
	sector2.setPosition(x, y);
	sector3.setPosition(x, y);
	sector4.setPosition(x, y);
	holo.setPosition(x, y);
}

void MiniMap::addDot(sf::Vector2f pos, sf::Color color)
{
	sf::Transform transform;
	transform.scale(getScale().x * 0.068, getScale().y * 0.068);
	transform.rotate(getRotation() - 45);
	sf::Sprite dot;
	dot.setTexture(mapTexture);
	dot.setTextureRect(sf::IntRect(501, 603, 27, 27));
	dot.setScale(getScale().x, getScale().y);
	dot.setColor(color);

	pos = transform.transformPoint(pos);
	pos.x += getPosition().x;
	pos.y += getPosition().y;
	sf::FloatRect rect = dot.getLocalBounds();
	dot.setOrigin(rect.width / 2, rect.height / 2);
	dot.setPosition(pos);
	radarDots.push_back(dot);
}

void MiniMap::setScale(sf::Vector2f scale)
{
	setScale(scale.x, scale.y);
}

void MiniMap::setScale(float x, float y)
{
	sf::Transformable::setScale(x, y);
	sector1.setScale(x, y);
	sector2.setScale(x, y);
	sector3.setScale(x, y);
	sector4.setScale(x, y);
	holo.setScale(x * 1.5, y * 1.5);
}

void MiniMap::notify(sf::Vector2f pos, sf::Color color)
{
	sf::Transform transform;
	transform.scale(getScale().x * 0.068, getScale().y * 0.068);
	transform.rotate(getRotation() - 45);
	sf::Sprite circle;
	circle.setTexture(mapTexture);
	circle.setTextureRect(sf::IntRect(501, 466, 91, 91));
	circle.setScale(getScale().x * 0.2, getScale().y * 0.2);
	circle.setColor(color);

	pos = transform.transformPoint(pos);
	pos.x += getPosition().x;
	pos.y += getPosition().y;
	sf::FloatRect rect = circle.getLocalBounds();
	circle.setOrigin(rect.width / 2, rect.height / 2);
	circle.setPosition(pos);
	radarAlerts.push_back(circle);
}

void MiniMap::setSectorColor(int sector, sf::Color color)
{
	switch (sector)
	{
	case 1:
		sector1.setColor(color);
		break;
	case 2:
		sector2.setColor(color);
		break;
	case 3:
		sector3.setColor(color);
		break;
	case 4:
		sector4.setColor(color);
		break;
	default:
		break;
	}
}
