#include "stdafx.h"
#include "AudioManager.h"



AudioManager::AudioManager()
{
}


AudioManager::~AudioManager()
{
}

void AudioManager::Shutdown()
{
	{
		auto iter = mapptrAudio.begin();
		while (iter != mapptrAudio.end())
		{
			delete (iter->second);
			++iter;
		}
		mapptrAudio.clear();
	}

	/*{
		auto iter = mapptrMusic.begin();
		while (iter != mapptrMusic.end())
		{
			delete (iter->second);
			++iter;
		}
		mapptrMusic.clear();
	}*/
}

sf::Sound * AudioManager::CreateSound(const std::string & ptrFilePath) //Takes a soundfile, adds it to a explosionBuffer pointer
{
	auto iter = mapptrAudio.find(ptrFilePath); //Creates an iterator that goes through the map via searching for the argument
	if (iter == mapptrAudio.end()) //If it reaches the end of the map
	{
		if (soundBuffer.loadFromFile(ptrFilePath.c_str()))  //Attempts to load a file to the sound explosionBuffer
		{
			sf::Sound* xSounds = new sf::Sound(soundBuffer); //Creates a new sound and pointer from the file in the soundbuffer
			mapptrAudio.insert(std::pair<std::string, sf::Sound*>(ptrFilePath, xSounds)); //Inserts the new sound and filename into the map
			iter = mapptrAudio.find(ptrFilePath); //Sets the iterator to this newly created map point
		}
		else
		{
			return nullptr;
		}
	}
	sf::Sound* xSound = (iter->second); //Creates a pointer for the created sound
	return xSound; //Returns the pointer, ends the scope of the iterator
}

/*sf::Music * AudioManager::CreateMusic(const std::string & ptrFilePath)//sf::Music streams the audio from a file rather than loading it to memory, this entire function is probably not needed.
{
	auto iter = mapptrMusic.find(ptrFilePath);
	if (iter == mapptrMusic.end())
	{
		sf::Music* xMusics;
		
		if (xMusics->openFromFile(ptrFilePath))
		{
			xMusics = new sf::Music();
			mapptrMusic.insert(std::pair<std::string, sf::Music*>(ptrFilePath, xMusics));
			iter = mapptrMusic.find(ptrFilePath);
		}
	}
	sf::Music* xMusic = (iter->second);
	return xMusic; 
}*/

void AudioManager::DestroySound(const std::string & ptrFilePath)
{
	auto iter = mapptrAudio.begin();
	while (iter != mapptrAudio.end())
	{
		if ((iter->first) == ptrFilePath)
		{
			// Needs to be here?(iter->second);
			mapptrAudio.erase(iter);
			return;
		}

		++iter;
	}
}

/*void AudioManager::DestroyMusic(const std::string & ptrFilePath) //sf::Music streams the audio from a file rather than loading it to memory, this entire function is probably not needed.
{
	auto iter = mapptrMusic.begin();
	while (iter != mapptrMusic.end())
	{
		if ((iter->first) == ptrFilePath)
		{
			(iter->second); //What? Is this needed? I'm not sure!
			mapptrMusic.erase(iter);
			return;
		}

		++iter;
	}
}*/
