#include "Projectile.h"
#include "ProjectileManager.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <SFML/Graphics/RenderTarget.hpp>


Projectile::Projectile(sf::Vector2f pos, float rotation, sf::Texture &projtext)
{
	setPosition(pos);
	setRotation(rotation);
	projectileSprite.setTexture(projtext);
	projectileSprite.setTextureRect(sf::IntRect(60, 0, 15, 78));
	projectileSprite.setOrigin(projectileSprite.getLocalBounds().width / 2, 0);
	projectileSprite.setPosition(pos);
	projectileSprite.setRotation(rotation + 90);
	destroyed = false;

	lifeTimeTimer = 3;
}

Projectile::~Projectile()
{
}

void Projectile::Update(float dt)
{
	lifeTimeTimer -= dt;
	{
		if (!destroyed && lifeTimeTimer >= 0)
		{
			float speed = 3000 * dt;
			float angle = getRotation() * M_PI / 180;
			move(cos(angle) * speed, sin(angle) * speed);
			projectileSprite.setPosition(getPosition());
			projectileSprite.setRotation(getRotation() + 90);
		}
		else
		{
			Destroy();
		}
	}

}

void Projectile::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	if (!destroyed)
	{
		target.draw(projectileSprite, states);
	}
}

sf::CircleShape Projectile::getCollider()
{
	sf::CircleShape collider;
	collider.setPosition(getPosition());
	collider.setRadius(projectileSprite.getLocalBounds().width / 2);
	return collider;
}

int Projectile::getStrenght()
{
	return 1;
}

void Projectile::Destroy()
{
	destroyed = true;
}

bool Projectile::isDestroyed() const
{
	return destroyed;
}

PROJECTILETYPE Projectile::GetProjectileType()
{
	return PROJ_BULLET;
}
