#pragma once
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <vector>
#include <SFML/Audio/SoundBuffer.hpp>
#include <SFML/Audio/Sound.hpp>
#include "AnimatedSprite.h"
#include <SFML/Graphics/Texture.hpp>

enum PROJECTILETYPE
{
	PROJ_BULLET,
	PROJ_MISSILE,
	PROJ_EMP,
	PROJ_FREEZE,
	PROJ_FREEZPLOSION
};
class EnemyManager;
class Projectile;

class ProjectileManager
{
public:
	ProjectileManager(EnemyManager* enManager); // TODO: Add EnemyManager here
	~ProjectileManager();
	void AddProjectile(PROJECTILETYPE type, sf::Vector2f pos, float angle);
	void Update(float dt);
	void Draw(sf::RenderWindow* window);
private:
	sf::Texture freezeplosionTexture;
	sf::Texture freezeTexture;
	std::vector<sf::IntRect> freezeFrames;
	sf::Sprite projectileSprite;
	sf::Texture projectileTexture;
	EnemyManager* enemyManager;
	std::vector<Projectile*> projectiles;
	std::vector<AnimatedSprite*> projFX;
	std::vector<sf::IntRect> muzzleFlashFrames;
	std::vector<sf::IntRect> impactFrames;
	sf::Texture impactTexture;
	sf::Texture muzzleFlashTexture;
	sf::SoundBuffer bufferShot;
	sf::SoundBuffer bufferMissile;
	sf::SoundBuffer bufferEMP;
	sf::Sound shotSFX;
	sf::Sound missileSFX;
	sf::Sound EMPSFX;
	// TODO: Add pointer to enemymanager
};
