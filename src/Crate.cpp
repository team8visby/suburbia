#include "Crate.h"
#include <SFML/Graphics/RenderTarget.hpp>
#define _USE_MATH_DEFINES
#include <math.h>

Crate::Crate(sf::Texture* crateTexture, int powerupType, float anglePos, float radiusPos)
{
	landAnimation.setTexture(*crateTexture);
	landAnimation.setLoopEnabled(false);
	fallAnimation.setTexture(*crateTexture);
	fallAnimation.setLoopEnabled(true);
	std::vector<sf::IntRect> fallFrames;
	fallFrames.push_back(sf::IntRect(246, 984, 244, 244));
	fallFrames.push_back(sf::IntRect(738, 738, 244, 244));
	fallFrames.push_back(sf::IntRect(738, 492, 244, 244));
	fallFrames.push_back(sf::IntRect(738, 246, 244, 244));
	fallFrames.push_back(sf::IntRect(738, 0, 244, 244));
	fallFrames.push_back(sf::IntRect(492, 984, 244, 244));
	fallFrames.push_back(sf::IntRect(492, 738, 244, 244));
	fallFrames.push_back(sf::IntRect(492, 492, 244, 244));
	fallFrames.push_back(sf::IntRect(492, 246, 244, 244));
	fallFrames.push_back(sf::IntRect(492, 0, 244, 244));
	fallAnimation.setAnimationRects(fallFrames);
	std::vector<sf::IntRect> landFrames;
	landFrames.push_back(sf::IntRect(738, 984, 244, 244));
	landFrames.push_back(sf::IntRect(246, 738, 244, 244));
	landFrames.push_back(sf::IntRect(246, 492, 244, 244));
	landFrames.push_back(sf::IntRect(246, 246, 244, 244));
	landFrames.push_back(sf::IntRect(246, 0, 244, 244));
	landFrames.push_back(sf::IntRect(0, 984, 244, 244));
	landFrames.push_back(sf::IntRect(0, 738, 244, 244));
	landFrames.push_back(sf::IntRect(0, 492, 244, 244));
	landFrames.push_back(sf::IntRect(0, 246, 244, 244));
	landFrames.push_back(sf::IntRect(0, 0, 244, 244));
	landAnimation.setAnimationRects(landFrames);
	auto rectAni = landAnimation.getLocalBounds();
	landAnimation.setOrigin(rectAni.width / 2, rectAni.height * .9);
	landAnimation.setFrameRate(4);
	fallAnimation.setOrigin(rectAni.width / 2, rectAni.height * .9);
	fallAnimation.setFrameRate(4);
	this->anglePos = anglePos;
	this->radiusPos = radiusPos;
	this->powerupType = powerupType;
	destroyed = false;
	opened = false;
	despawnTimer = 0;
}

Crate::~Crate()
{
}

bool Crate::isOpened()
{
	return opened;
}

bool Crate::isDestroyed()
{
	return destroyed;
}

int Crate::getPowerupType()
{
	return powerupType;
}

float Crate::getAnglePos()
{
	return anglePos;
}

float Crate::getRadiusPos()
{
	return radiusPos;
}

void Crate::destroy()
{
	destroyed = true;
}

void Crate::update(float dt)
{
	if (opened)
	{
		despawnTimer += dt;
		landAnimation.updateAnimation(dt);
		auto rect = landAnimation.getLocalBounds();
		landAnimation.setOrigin(rect.width / 2, rect.height - 42);
		if (despawnTimer >= 10)
		{
			destroy();
		}
	}
	else
	{
		if (radiusPos <= 2048)
		{
			radiusPos = 2048;
			opened = true;
		}
		else
		{
			radiusPos -= 100 * dt;
		}

		float angleRadians = (anglePos - 90) * M_PI / 180;
		setPosition(cos(angleRadians)*radiusPos, sin(angleRadians)*radiusPos);
		setRotation(anglePos);
		fallAnimation.setPosition(getPosition());
		fallAnimation.setRotation(anglePos);
		fallAnimation.updateAnimation(dt);
		landAnimation.setPosition(getPosition());
		landAnimation.setRotation(anglePos);
	}
}

void Crate::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	if (opened)
	{
		target.draw(landAnimation, states);
	}
	else
	{
		target.draw(fallAnimation, states);
	}
}
