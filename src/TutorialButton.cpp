#include "TutorialButton.h"

TutorialButton::TutorialButton(sf::Vector2f pos, float rotation, sf::Sprite sText, sf::Keyboard::Key key)
{
	assignedKey = key;
	buttonSprite=sText;
	buttonSprite.setPosition(pos);
	buttonSprite.setRotation(rotation);
	isShown = true;
}

TutorialButton::TutorialButton(sf::Vector2f pos, float rotation, sf::Sprite bSprite, sf::Mouse::Button button)
{
	assignedButton = button;
	buttonSprite = bSprite;
	buttonSprite.setPosition(pos);
	buttonSprite.setRotation(rotation);
	isShown = true;
}

TutorialButton::~TutorialButton()
{
}

void TutorialButton::Update(float dt)
{
	if (sf::Keyboard::isKeyPressed(assignedKey))
	{
		setShown(false);
	}

	if (sf::Mouse::isButtonPressed(assignedButton))
	{
		setShown(false);
	}
}

void TutorialButton::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	if (isShown)
	{
		target.draw(buttonSprite, states);
	}
}

bool TutorialButton::getShown()
{
	return isShown;
}

void TutorialButton::setShown(bool shown)
{
	isShown = shown;
}
