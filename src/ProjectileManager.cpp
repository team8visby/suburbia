#include "ProjectileManager.h"
#include "EnemyManager.h"
#include "Projectile.h"
#include "Missile.h"
#include "EMP.h"
#include "FreezeShot.h"
#include "FreezeExplosion.h"

ProjectileManager::ProjectileManager(EnemyManager* enManager)
{
	enemyManager = enManager;
	projectileTexture.loadFromFile("../assets/Sprites/projectiles.png");
	bufferShot.loadFromFile("../assets/Sound Files/gun-for-lui2-Ver1.wav");//explosionBuffer bullet sfx into memory
	shotSFX.setBuffer(bufferShot);
	bufferMissile.loadFromFile("../assets/Sound Files/warpout_ver1.wav");//explosionBuffer missile sfx into memory
	missileSFX.setBuffer(bufferMissile);
	muzzleFlashTexture.loadFromFile("../assets/Boom smoke.png");
	impactTexture.loadFromFile("../assets/Sprites/Impact.png");
	muzzleFlashFrames.push_back(sf::IntRect(0, 0, 76, 129));
	muzzleFlashFrames.push_back(sf::IntRect(77, 0, 76, 129));
	muzzleFlashFrames.push_back(sf::IntRect(154, 0, 76, 129));
	muzzleFlashFrames.push_back(sf::IntRect(231, 0, 76, 129));
	muzzleFlashFrames.push_back(sf::IntRect(308, 0, 76, 129));
	muzzleFlashFrames.push_back(sf::IntRect(385, 0, 76, 129));
	muzzleFlashFrames.push_back(sf::IntRect(0, 130, 76, 129));
	muzzleFlashFrames.push_back(sf::IntRect(77, 130, 76, 129));

	freezeplosionTexture.loadFromFile("../assets/Cryo wave.png");
	freezeTexture.loadFromFile("../assets/Sprites/Freeze Sprite.png");

	impactFrames.push_back(sf::IntRect(0, 0, 73, 98));
	impactFrames.push_back(sf::IntRect(74, 0, 73, 98));
	impactFrames.push_back(sf::IntRect(148, 0, 73, 98));
	impactFrames.push_back(sf::IntRect(0, 99, 73, 98));
	impactFrames.push_back(sf::IntRect(74, 99, 73, 98));

	bufferEMP.loadFromFile("../assets/Sound Files/77172__supraliminal__pulse-laser_ver1.wav");
	EMPSFX.setBuffer(bufferEMP);
}

ProjectileManager::~ProjectileManager()
{
	{
		auto it = projFX.begin();
		while (it != projFX.end())
		{
			delete (*it);
			++it;
		}
	}
	{
		auto it = projectiles.begin();
		while (it != projectiles.end())
		{
			delete (*it);
			++it;
		}
	}
}

void ProjectileManager::AddProjectile(PROJECTILETYPE type, sf::Vector2f pos, float angle)
{
	AnimatedSprite * flash = new AnimatedSprite();
	flash->setTexture(muzzleFlashTexture);
	flash->setFrameRate(30);
	flash->setLoopEnabled(false);
	flash->setAnimationRects(muzzleFlashFrames);
	flash->setPosition(pos);
	flash->setRotation(angle + 90);
	flash->setScale(1.6, 1.6);
	switch (type)
	{
	case PROJ_BULLET:
		flash->setOrigin(38, 146);
		projFX.push_back(flash);
		projectiles.push_back(new Projectile(pos, angle, projectileTexture));
		shotSFX.setPitch(rand() % 3 * 0.1f + 1);
		shotSFX.play();
		break;
	case PROJ_MISSILE:
		flash->setOrigin(38, 140);
		projFX.push_back(flash);
		missileSFX.setPitch(rand() % 3 * 0.1f + 1);
		missileSFX.play();
		projectiles.push_back(new Missile(pos, angle, enemyManager, projectileTexture));
		break;
	case PROJ_EMP:
		EMPSFX.play();
		projectiles.push_back(new EMP(pos, angle, enemyManager, projectileTexture));
		break;
	case PROJ_FREEZE:
		projectiles.push_back(new FreezeShot(pos, angle, enemyManager, freezeTexture));
		break;
	case PROJ_FREEZPLOSION:
		projectiles.push_back(new FreezeExplosion(pos, angle, enemyManager, freezeplosionTexture));
	default:
		break;
	}
}

void ProjectileManager::Update(float dt)
{
	std::vector<sf::Vector2f> freezeSplosions;
	{
		auto it = projectiles.begin();
		while (it != projectiles.end())
		{
			(*it)->Update(dt);
			enemyManager->IsHit((*it));
			
			if ((*it)->isDestroyed())
			{
				if ((*it)->GetProjectileType()==PROJ_FREEZE)
				{
					freezeSplosions.push_back((*it)->getPosition());
					//AddProjectile(PROJ_FREEZPLOSION, (*it)->getPosition(), (*it)->getRotation()); // Problem h�r, g�r utanf�r iteratorn pls
				}
				if ((*it)->GetProjectileType() == PROJ_BULLET)
				{
					AnimatedSprite * impact = new AnimatedSprite();
					impact->setTexture(impactTexture);
					impact->setLoopEnabled(false);
					impact->setFrameRate(30);
					impact->setAnimationRects(impactFrames);
					impact->setOrigin(36.5, 0);
					impact->setPosition((*it)->getPosition());
					impact->setRotation((*it)->getRotation() + 90);
					projFX.push_back(impact); 
				}
				delete (*it);
				it = projectiles.erase(it);
			}
			else
			{
				++it;
			}
		}
	}
	{
		auto it = freezeSplosions.begin();
		while (it != freezeSplosions.end())
		{
			AddProjectile(PROJ_FREEZPLOSION, (*it), 0);
			it = freezeSplosions.erase(it);
		}
	}
	{
		auto it = projFX.begin();
		while (it != projFX.end())
		{
			(*it)->updateAnimation(dt);
			if ((*it)->isPlaybackFinished())
			{
				delete (*it);
				it = projFX.erase(it);
			}
			else
			{
				++it;
			}
		}
	}
}

void ProjectileManager::Draw(sf::RenderWindow* window)
{
	{
		auto it = projectiles.begin();
		while (it != projectiles.end())
		{
			window->draw(*(*it));
			++it;
		}
	}
	{
		auto it = projFX.begin();
		while (it != projFX.end())
		{
			window->draw(*(*it));
			++it;
		}
	}
}
