#include "stdafx.h"
#include "EnemyManager.h"
#include "PowerUpManager.h"
#include "AudioManager.h"
#include "EnemyShip.h"
#include "Projectile.h"
#include "CollisionManager.h"
#include "BuildingManager.h"
#include "AnimatedSprite.h"
#include "MiniMap.h"
#include "HeadsUpDisplay.h"

EnemyManager::EnemyManager(Planet* planet, AudioManager* getPtrAudioManager, PowerUpManager* getPtrPowerUpManager, BuildingManager* acqBuildingManager, MiniMap * map, int* score)
{
	miniMap = map;
	ptrAudioManager = getPtrAudioManager;
	ptrPowerUpManager = getPtrPowerUpManager;
	ptrBuildingManager = acqBuildingManager;
	EnemyManager::planet = planet;
	hasLanded = false;
	roundEnd = true;
	shipsLanded = 0;
	this->score = score;

	explosionTexture.loadFromFile("../assets/explosion.png");
	explosionFrames.push_back(sf::IntRect(0, 0, 192, 192));
	explosionFrames.push_back(sf::IntRect(0, 194, 152, 150));
	explosionFrames.push_back(sf::IntRect(94, 347, 82, 91));
	explosionFrames.push_back(sf::IntRect(0, 347, 92, 102));
	explosionFrames.push_back(sf::IntRect(339, 0, 120, 124));
	explosionFrames.push_back(sf::IntRect(305, 288, 133, 134));
	explosionFrames.push_back(sf::IntRect(305, 146, 138, 140));
	explosionFrames.push_back(sf::IntRect(194, 0, 143, 144));
	explosionFrames.push_back(sf::IntRect(154, 194, 149, 151));

	dustTexture.loadFromFile("../assets/Land smoke.png");
	dustFrames.push_back(sf::IntRect(0, 0, 122, 120));
	dustFrames.push_back(sf::IntRect(123, 0, 122, 120));
	dustFrames.push_back(sf::IntRect(0, 121, 122, 120));
	dustFrames.push_back(sf::IntRect(123, 121, 122, 120));
	dustFrames.push_back(sf::IntRect(246, 0, 122, 120));
	dustFrames.push_back(sf::IntRect(246, 121, 122, 120));
	dustFrames.push_back(sf::IntRect(369, 0, 122, 120));
	dustFrames.push_back(sf::IntRect(369, 121, 122, 120));

	iceCubeTexture.loadFromFile("../assets/Cryo Cube.png");


	explosionBuffer.loadFromFile("../assets/Sound Files/94185__nbs-dark__explosion_ver1.wav");//change to something that we can use
	floatingBuffer1.loadFromFile("../assets/Sound Files/radar1_ver3.wav");
	floatingBuffer2.loadFromFile("../assets/Sound Files/radar2_ver2.wav");
	floatingBuffer3.loadFromFile("../assets/Sound Files/radar3_ver2.wav");
	landedBuffer.loadFromFile("../assets/Sound Files/204614__pikachu09__door-slam.wav");
	explosionSFX.setBuffer(explosionBuffer);
	floatingSFX1.setBuffer(floatingBuffer1);
	floatingSFX2.setBuffer(floatingBuffer2);
	floatingSFX3.setBuffer(floatingBuffer3);
	landedSFX.setBuffer(landedBuffer);
	enemy1Texture.loadFromFile("../assets/Sprites/alien 1.png");
	enemy2Texture.loadFromFile("../assets/Sprites/alien 2.png");
	enemy3Texture.loadFromFile("../assets/Sprites/alien 3.png");

	hitBuffer.loadFromFile("../assets/Sound Files/329122__jessieyun0404__jessieyun0404-banging-desk-with-a-hand_ver2.wav");
	hitSFX.setBuffer(hitBuffer);
}

EnemyManager::~EnemyManager()
{
	auto it = vecPtrEnemyShip.begin();
	while (it != vecPtrEnemyShip.end())
	{
		delete (*it);
		it = vecPtrEnemyShip.erase(it);
	}
}

void EnemyManager::Initialize(int wave)
{
	spawnTimer = 0;
	int totalEnemies = 4 + (wave * wave) / 3;
	int sector = floor((wave / 2) % 4);
	int angleOffset = 90 * sector;
	switch (sector)
	{
	case 0:
		miniMap->notify(sf::Vector2f(2048, -2048), sf::Color(255, 0, 0, 255));
		break;
	case 1:
		miniMap->notify(sf::Vector2f(2048, 2048), sf::Color(255, 0, 0, 255));
		break;
	case 2:
		miniMap->notify(sf::Vector2f(-2048, 2048), sf::Color(255, 0, 0, 255));
		break;
	case 3:
		miniMap->notify(sf::Vector2f(-2048, -2048), sf::Color(255, 0, 0, 255));
		break;
	default:
		break;
	}
	for (int i = 0; i < totalEnemies; i++)
	{
		Spawn spawn;
		spawn.positionAngle = angleOffset + 10 + rand() % 70;
		switch (rand() % 3)
		{
		case 1:
			spawn.shipType = SHIP_TYPE1;
			break;
		case 2:
			spawn.shipType = SHIP_TYPE2;
			break;
		case 3:
		default:
			spawn.shipType = SHIP_TYPE3;
			break;
		}
		spawn.spawnDelay = i * 1.2 + (rand() % 5);
		vecSpawnEnemyShip.push_back(spawn);
	}
	for (int i = 0; i < std::max(1, totalEnemies / 3); i++)
	{
		Spawn spawn;
		spawn.positionAngle = angleOffset + 10 + rand() % 70;
		spawn.shipType = SHIPNOTRLLY_POWERUP;
		float powerRand = rand() % 100;
		if (powerRand < 20)
		{
			spawn.powerType = POWER_EMP;
		}
		else if (powerRand < 60)
		{
			spawn.powerType = POWER_ROCKETS;
		}
		else
		{
			spawn.powerType = POWER_FREEZE;
		}
		spawn.spawnDelay = (rand() % totalEnemies) * 1.2 + (rand() % 5);
		vecSpawnEnemyShip.push_back(spawn);
	}
	roundEnd = false;
}

void EnemyManager::CreateShip()
{
	float enemyRadiusPos = 3200.0f;
	auto it = vecSpawnEnemyShip.begin();
	while (it != vecSpawnEnemyShip.end())
	{
		roundEnd = false;
		if ((*it).spawnDelay < spawnTimer)
		{
			switch ((*it).shipType)
			{
			case SHIP_TYPE1:
				vecPtrEnemyShip.push_back(new EnemyShip(enemy2Texture, iceCubeTexture, planet, (*it).shipType, rand(), (*it).positionAngle, enemyRadiusPos));
				break;
			case SHIP_TYPE2:
				vecPtrEnemyShip.push_back(new EnemyShip(enemy1Texture, iceCubeTexture, planet, (*it).shipType, rand(), (*it).positionAngle, enemyRadiusPos));
				break;
			case SHIP_TYPE3:
				vecPtrEnemyShip.push_back(new EnemyShip(enemy3Texture, iceCubeTexture, planet, (*it).shipType, rand(), (*it).positionAngle, enemyRadiusPos));
				vecPtrEnemyShip.push_back(new EnemyShip(enemy3Texture, iceCubeTexture, planet, SHIP_TYPE4, rand(), (*it).positionAngle, enemyRadiusPos));
				vecPtrEnemyShip.push_back(new EnemyShip(enemy3Texture, iceCubeTexture, planet, SHIP_TYPE4, rand(), (*it).positionAngle, enemyRadiusPos));
				break;
			case SHIP_TYPE4:
				vecPtrEnemyShip.push_back(new EnemyShip(enemy3Texture, iceCubeTexture, planet, (*it).shipType, rand(), (*it).positionAngle, enemyRadiusPos));
				break;
			case SHIPNOTRLLY_POWERUP:
				ptrPowerUpManager->CreatePowerup((*it).powerType, (*it).positionAngle, enemyRadiusPos);
				break;
			default:
				break;
			}
			vecSpawnEnemyShip.erase(it);
			return;
		}
		it++;
	}
}



void EnemyManager::Update(float deltaTime, float& marketValue, HeadsUpDisplay * hud)
{
	spawnTimer += deltaTime;
	CreateShip();
	CheckAudio();

	if (vecSpawnEnemyShip.size() == 0)
	{
		roundEnd = true;
	}
	else
	{
		roundEnd = false;
	}
	auto it = vecPtrEnemyShip.begin();
	while (it != vecPtrEnemyShip.end())
	{
		if (!(*it)->hasLanded() && (*it)->IsActive())
		{
			(*it)->Update(deltaTime);
			roundEnd = false;
			if (!(*it)->IsFrozen())
			{

				if ((*it)->hasLanded())
				{
					miniMap->notify((*it)->getPosition(), sf::Color(255, 55, 55, 200));
					float shipRadius = (*it)->getCollider().getRadius();
					sf::Vector2f shipPos = (*it)->getPosition();
					float shipRot = (*it)->getRotation();
					AnimatedSprite dustR;
					AnimatedSprite dustL;
					dustR.setTexture(dustTexture);
					dustR.setAnimationRects(dustFrames);
					dustR.setLoopEnabled(false);
					dustR.setOrigin(-shipRadius * 3, dustR.getLocalBounds().height);
					dustR.setPosition(shipPos);
					dustR.setRotation(shipRot);
					dustR.setFrameRate(10);
					dustR.setScale(2.5, 2);
					dustL = AnimatedSprite(dustR);
					dustL.setScale(-2.5, 2);
					effects.push_back(dustR);
					effects.push_back(dustL);
					marketValue -= (*it)->getCost();
					hud->dmgNumber(-(*it)->getCost());
					ptrBuildingManager->CheckBuilding((*it));
					landedSFX.play();
					if ((*it)->GetType() == SHIP_TYPE1)
					{
						floatingSFX1.setLoop(false);
						floatingSFX1.stop();
					}
					if ((*it)->GetType() == SHIP_TYPE2)
					{
						floatingSFX2.setLoop(false);
						floatingSFX2.stop();
					}
					if ((*it)->GetType() == SHIP_TYPE3)
					{
						floatingSFX3.setLoop(false);
						floatingSFX3.stop();
					}
					if ((*it)->GetType() == SHIP_TYPE4)
					{
						floatingSFX3.setLoop(false);
						floatingSFX3.stop();
					}
				}
			}
			else
			{
			//frozenSprite.updateAnimation(deltaTime);
			}
			miniMap->addDot((*it)->getPosition(), sf::Color(255, 0, 0, 200));
		}
		++it;

	}
	{ // Effects
		auto it = effects.begin();
		while (it != effects.end())
		{
			(*it).updateAnimation(deltaTime);
			(*it).setOrigin((*it).getLocalBounds().width / 2, (*it).getLocalBounds().height / 2);
			if ((*it).isPlaybackFinished())
			{
				it = effects.erase(it);
			}
			else
			{
				++it;
			}
		}
	}
}

EnemyShip* EnemyManager::GetShipInRange(sf::Vector2f pos, float range)
{
	EnemyShip* closest = nullptr;
	float closestDist = range;
	auto it = vecPtrEnemyShip.begin();
	while (it != vecPtrEnemyShip.end())
	{
		if ((*it)->hasLanded() || (*it)->isDestroyed())
		{
			++it;
			continue;
		}
		sf::Vector2f enPos = (*it)->getPosition();
		sf::Vector2f deltaPos = pos - enPos;
		float distance = sqrt(deltaPos.x * deltaPos.x + deltaPos.y * deltaPos.y);

		if (closestDist >= distance)
		{
			closestDist = distance;
			closest = (*it);
		}

		++it;
	}
	return closest;
}

void EnemyManager::IsHit(Projectile* proj)
{
	auto it = vecPtrEnemyShip.begin();
	while (it != vecPtrEnemyShip.end())
	{
		if (!(*it)->hasLanded() && (*it)->IsActive())
		{
			bool hit = CollisionManager::CheckCircles(proj->getCollider(), (*it)->getCollider());
			if (hit)
			{
				AnimatedSprite iceCube;
				switch (proj->GetProjectileType())
				{
				case PROJ_FREEZPLOSION:
					if (!(*it)->IsFrozen())
					{
						(*it)->SetFreezeTimer(5);
					}
					break;
				case PROJ_EMP:
					(*it)->hit(proj->getStrenght());
					break;
				default:
					proj->Destroy();
					(*it)->hit(proj->getStrenght());
					break;
				}
				if ((*it)->isDestroyed())
				{
					explosionSFX.setPitch(rand() % 3 * 0.05f + 1);
					explosionSFX.play();
					if (proj->GetProjectileType() != PROJ_EMP)
					{

						if ((*it)->GetType() == SHIP_TYPE1)
						{
							floatingSFX1.setLoop(false);
							floatingSFX1.stop();
						}
						if ((*it)->GetType() == SHIP_TYPE2)
						{
							floatingSFX2.setLoop(false);
							floatingSFX2.stop();
						}
						if ((*it)->GetType() == SHIP_TYPE3)
						{
							floatingSFX3.setLoop(false);
							floatingSFX3.stop();
						}
						if ((*it)->GetType() == SHIP_TYPE4)
						{
							floatingSFX3.setLoop(false);
							floatingSFX3.stop();
						}
					}
					if ((*it)->GetType() == SHIP_TYPE3)
					{
						int shipAmont = 0;
						bool firstCreated = false;

						auto it2 = vecPtrEnemyShip.begin();
						while (it2 != vecPtrEnemyShip.end())
						{

							if ((*it2)->GetType() == SHIP_TYPE4 && (*it2)->IsActive() == false)
							{
								if (firstCreated == false)
								{
									(*it2)->Activate((*it)->GetRelativeAngle(), (*it)->GetRelativeRadius());
									++shipAmont;
									firstCreated = true;
									++it2;
								}
								if (firstCreated == true)
								{
									(*it2)->Activate((*it)->GetRelativeAngle() - 2, (*it)->GetRelativeRadius() - 2);
									++shipAmont;

								}
							}

							if (shipAmont >= 2)
							{
								//exit this while loop and continue the previous loop
								goto JUMP;
							}

							it2++;
						}

					}
					else
					{
						(*score) += (*it)->getCost();
					}
				JUMP:
					AnimatedSprite explosion;
					explosion.setTexture(explosionTexture);
					explosion.setFrameRate(30);
					explosion.setAnimationRects(explosionFrames);
					explosion.setPosition((*it)->getPosition());
					explosion.setLoopEnabled(false);
					explosion.setRotation(rand() % 360);
					explosion.scale(1.6, 1.6);
					effects.push_back(explosion);
					it = vecPtrEnemyShip.erase(it);
					continue;
				}
				else
				{
					hitSFX.setVolume(60);
					hitSFX.play();
				}
			}
		}
		++it;
	}
}

void EnemyManager::Draw(sf::RenderWindow* window)
{
	{
		auto it = vecPtrEnemyShip.begin();
		while (it != vecPtrEnemyShip.end())
		{
			window->draw(*(*it));
			++it;
		}
	}
	{
		auto it = effects.begin();
		while (it != effects.end())
		{
			window->draw((*it));
			++it;
		}
	}
}

bool EnemyManager::hasRoundEnded()
{
	return roundEnd;
}

int EnemyManager::ShipsLanded()
{
	return shipsLanded;
}

std::vector<EnemyShip*> EnemyManager::getShipVector()
{
	return vecPtrEnemyShip;
}

bool EnemyManager::HasLanded()
{
	return hasLanded;
}

void EnemyManager::CheckAudio()
{
	auto it = vecPtrEnemyShip.begin();
	while (it != vecPtrEnemyShip.end())
	{
		if ((*it)->GetType() == SHIP_TYPE1 && (*it)->hasLanded() == false)
		{
			if (floatingSFX1.getLoop() == false)
			{
				floatingSFX1.setVolume(25);
				floatingSFX1.play();
				floatingSFX1.setLoop(true);
			}

		}
		if ((*it)->GetType() == SHIP_TYPE2 && (*it)->hasLanded() == false)
		{
			if (floatingSFX2.getLoop() == false)
			{
				floatingSFX2.setVolume(75);
				floatingSFX2.play();
				floatingSFX2.setLoop(true);
			}
		}
		if ((*it)->GetType() == SHIP_TYPE3 && (*it)->hasLanded() == false)
		{
			if (floatingSFX3.getLoop() == false)
			{
				floatingSFX3.setVolume(75);
				floatingSFX3.play();
				floatingSFX3.setLoop(true);
			}
		}
		if ((*it)->GetType() == SHIP_TYPE4 && (*it)->hasLanded() == false)
		{
			if (floatingSFX3.getLoop() == false)
			{
				floatingSFX3.setVolume(75);
				floatingSFX3.play();
				floatingSFX3.setLoop(true);
			}
		}
		it++;
	}
}
