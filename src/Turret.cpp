#include "Turret.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "Toolbox.h"
#include "EnemyShip.h"
#include <SFML/Graphics/RenderTarget.hpp>
#include "ProjectileManager.h"

Turret::Turret(sf::Vector2f pos, float rotation, ProjectileManager* projManager)
{
	target = nullptr;
	projectileManager = projManager;
	turretTexture.loadFromFile("../assets/Sprites/Turret sprite.png");
	feetSprite.setTexture(turretTexture);
	feetSprite.setTextureRect(sf::IntRect(0, 0, 163, 80));
	cannonSprite.setTexture(turretTexture);
	cannonSprite.setTextureRect(sf::IntRect(164, 0, 72, 140));
	cannonPipeSprite.setTexture(turretTexture);
	cannonPipeSprite.setTextureRect(sf::IntRect(0, 81, 27, 91));
	range = 700;

	auto baseSize = feetSprite.getLocalBounds();
	feetSprite.setOrigin(baseSize.width / 2, baseSize.height / 2 + 200);
	auto cannonSize = cannonSprite.getLocalBounds();
	cannonSprite.setOrigin(cannonSize.width / 2, cannonSize.height + 400);
	auto pipeSize = cannonPipeSprite.getLocalBounds();
	cannonPipeSprite.setOrigin(pipeSize.width / 2, pipeSize.height + 800);

	setPosition(pos);
	setRotation(rotation);
	feetSprite.setPosition(pos);
	feetSprite.setRotation(rotation);

	float dX = cos((rotation - 90) * M_PI / 180) * 48;
	float dY = sin((rotation - 90) * M_PI / 180) * 48;
	cannonSprite.setPosition(pos.x + dX, pos.y + dY);
	cannonSprite.setRotation(rotation);
	cannonPipeSprite.setPosition(pos.x + dX, pos.y + dY);
	cannonPipeSprite.setRotation(rotation);

	spawnTimer = 1;
	fireRateTimer = 0;
}

Turret::~Turret()
{
}

void Turret::Update(float dt)
{
	fireRateTimer -= dt;
	if (spawnTimer <= 0)
	{
		if (target != nullptr)
		{
			float deltaX = cannonSprite.getPosition().x - target->getPosition().x;
			float deltaY = cannonSprite.getPosition().y - target->getPosition().y;
			float distance = sqrt(deltaX * deltaX + deltaY * deltaY);
			if (distance < range)
			{
				float rotation = std::atan2(deltaY, deltaX) * 180 / M_PI - 90;

				float rotationLess = rotation - 360;
				float rotationMore = rotation + 360;
				if (std::min(std::abs(rotationLess - cannonSprite.getRotation()), std::abs(rotationMore - cannonSprite.getRotation())) < std::abs(rotation - cannonSprite.getRotation()))
				{
					if (std::abs(rotationLess - cannonSprite.getRotation()) < std::abs(rotationMore - cannonSprite.getRotation()))
					{
						rotation = rotationLess;
					}
					else
					{
						rotation = rotationMore;
					}
				}

				cannonSprite.setRotation(Toolbox::lerp(cannonSprite.getRotation(), rotation, .1, dt));
				cannonPipeSprite.setRotation(cannonSprite.getRotation());
				Fire();
			}
		}
		auto pipeSize = cannonPipeSprite.getLocalBounds();
		sf::Vector2f pipeOrigin = Toolbox::lerp(cannonPipeSprite.getOrigin(),
			sf::Vector2f(pipeSize.width / 2, pipeSize.height + 80), 0.2, dt);
		cannonPipeSprite.setOrigin(pipeOrigin);
	}
	else
	{
		spawnTimer -= dt;
		auto baseSize = feetSprite.getLocalBounds();
		sf::Vector2f baseTargetPos = sf::Vector2f(baseSize.width / 2, baseSize.height / 2 + 24);
		feetSprite.setOrigin(Toolbox::lerp(feetSprite.getOrigin(), baseTargetPos, .002, dt));
		auto cannonSize = cannonSprite.getLocalBounds();
		sf::Vector2f cannonTargetPos = sf::Vector2f(cannonSize.width / 2, cannonSize.height - 16);
		cannonSprite.setOrigin(Toolbox::lerp(cannonSprite.getOrigin(), cannonTargetPos, .002, dt));
		auto pipeSize = cannonPipeSprite.getLocalBounds();
		sf::Vector2f pipeTargetPos = sf::Vector2f(pipeSize.width / 2, pipeSize.height + 80);
		cannonPipeSprite.setOrigin(Toolbox::lerp(cannonPipeSprite.getOrigin(), pipeTargetPos, .002, dt));
	}
}

void Turret::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(cannonPipeSprite, states);
	target.draw(cannonSprite, states);
	target.draw(feetSprite, states);
}

float Turret::getRange()
{
	return range;
}

sf::CircleShape Turret::getCollider()
{
	sf::CircleShape collider;
	collider.setRadius( feetSprite.getTextureRect().width / 2);
	collider.setPosition(getPosition());
	return collider;
}

void Turret::setTarget(EnemyShip* target)
{
	Turret::target = target;
}

void Turret::Fire()
{
	if (fireRateTimer <= 0)
	{
		fireRateTimer = .5;
		projectileManager->AddProjectile(PROJ_BULLET, cannonSprite.getPosition(), cannonSprite.getRotation() - 90);

		auto pipeSize = cannonPipeSprite.getLocalBounds();
		cannonPipeSprite.setOrigin(pipeSize.width / 2, pipeSize.height);

	}
}
