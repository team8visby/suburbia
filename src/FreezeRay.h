#pragma once
#include "stdafx.h"
#include "Projectile.h"
#include "EnemyManager.h"

class FreezeRay : public Projectile
{
public:
	FreezeRay(sf::Vector2f pos, float rotation, EnemyManager* enemymanager);
	PROJECTILETYPE GetProjectileType() override;
	void Update(float dt);
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	sf::CircleShape getCollider();

private:
	EnemyManager* enemyManager;
	sf::CircleShape rayShape;
	sf::Vector2f raysize;
	sf::IntRect rayform;
};
