#include "stdafx.h"
#include "TurretManager.h"
#include "EnemyManager.h"
#include "CollisionManager.h"
#include <SFML/Graphics/RenderWindow.hpp>
#include "MiniMap.h"

TurretManager::TurretManager(ProjectileManager* projManager , EnemyManager* enManager, MiniMap * map)
{
	projectileManager = projManager;
	enemyManager = enManager;
	disableTimer = 0;
	miniMap = map;

	shockTexture.loadFromFile("../assets/Sprites/emp sprite.png");
	shockSprite.setTexture(shockTexture);
	std::vector<sf::IntRect> shockAni;
	shockAni.push_back(sf::IntRect(0, 0, 375, 237));
	shockAni.push_back(sf::IntRect(0, 238, 375, 237));
	shockAni.push_back(sf::IntRect(376, 0, 375, 237));
	shockAni.push_back(sf::IntRect(376, 238, 375, 237));
	shockAni.push_back(sf::IntRect(0, 476, 375, 237));
	shockAni.push_back(sf::IntRect(0, 714, 375, 237));
	shockAni.push_back(sf::IntRect(376, 476, 375, 237));
	shockAni.push_back(sf::IntRect(376, 714, 375, 237));
	shockSprite.setAnimationRects(shockAni);
	shockSprite.setFrameRate(60);
	auto rect = shockSprite.getGlobalBounds();
	shockSprite.setOrigin(rect.width * 0.5, rect.height * 0.7);
	buffer.loadFromFile("../assets/Sound Files/qubodup-edev_ver1.wav");
	empSFX.setBuffer(buffer);
	spawnBuffer.loadFromFile("../assets/Sound Files/turretThunk_ver3.wav");
	spawnSFX.setBuffer(spawnBuffer);
	errorBuffer.loadFromFile("../assets/Sound Files/error_ver1.wav");
	errorSFX.setBuffer(errorBuffer);
	errPlay = false;
}

TurretManager::~TurretManager()
{
	auto it = turrets.begin();
	while (it != turrets.end())
	{
		delete (*it);
		it = turrets.erase(it);
	}
}

void TurretManager::AddTurret(sf::Vector2f pos, float rotation, float& marketValue)
{
	float turretCost = 500;
	if (marketValue > turretCost)
	{
		bool collides = false;
		for (auto it = turrets.begin(); it != turrets.end(); ++it)
		{
			sf::CircleShape collider = (*it)->getCollider();
			sf::CircleShape newCollider(collider);
			newCollider.setPosition(pos);

			if (CollisionManager::CheckCircles(newCollider, collider))
			{
				collides = true;
				break;
			}
		}
		if (!collides)
		{
			marketValue -= turretCost;
			turrets.push_back(new Turret(pos, rotation, projectileManager));
			spawnSFX.play();
		}
	}
	else
	{
		if (errPlay == false)
		{
			errorSFX.play();
			errPlay = true;
		}
	}
}

void TurretManager::Update(float dt)
{
	if (errPlay == true)
	{
		if (errorSFX.getStatus() == errorSFX.Stopped)
		{
			errPlay = false;
		}
	}
	if (disableTimer <= 0)
	{
		auto it = turrets.begin();
		while (it != turrets.end())
		{
			(*it)->setTarget(enemyManager->GetShipInRange((*it)->getPosition(), (*it)->getRange()));
			(*it)->Update(dt);
			miniMap->addDot((*it)->getPosition(), sf::Color(255, 166, 0, 200));
			++it;
		}
	}
	else
	{
		shockSprite.updateAnimation(dt);
		disableTimer -= dt;
	}
	if (empSFX.getLoop() == true)
	{
		if (sndLoop.getElapsedTime().asSeconds() >= 4)
		{
			empSFX.setLoop(false);
			sndLoop.restart();
		}
	}
}

void TurretManager::Draw(sf::RenderWindow* window)
{
	auto it = turrets.begin();
	while (it != turrets.end())
	{
		window->draw(*(*it));
		if (disableTimer > 0)
		{
			shockSprite.setPosition((*it)->getPosition());
			shockSprite.setRotation((*it)->getRotation());
			window->draw(shockSprite);
		}
		++it;
	}

}

void TurretManager::disableTurrets(float timeSec)
{
	disableTimer = timeSec;
	empSFX.setLoop(true);
	empSFX.play();
	sndLoop.restart();
}
