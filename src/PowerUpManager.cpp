#include "stdafx.h"
#include "PowerUpManager.h"
#include "PowerUp.h"
#include "Player.h"
#include "CollisionManager.h"
#include "MiniMap.h"
#include "Crate.h"

PowerUpManager::PowerUpManager(MiniMap * map)
{
	storedPowerType = POWER_NONE;
	buffer.loadFromFile("../assets/Sound Files/220173__gameaudio__spacey-1up-power-up.wav");
	powerupTexture.loadFromFile("../assets/Sprites/Power.png");
	crateTexture.loadFromFile("../assets/Sprites/box sprite.png");
	pickupSFX.setBuffer(buffer);
	miniMap = map;
	firstPower = false;
}

PowerUpManager::~PowerUpManager()
{
	{
		auto it = vecCrate.begin();
		while (it != vecCrate.end())
		{
			delete (*it);
			++it;
		}
	}
	{
		auto it = vecPowerUp.begin();
		while (it != vecPowerUp.end())
		{
			delete (*it);
			it++;
		}
	}
}

void PowerUpManager::ClearPowerUp()
{
	storedPowerType = POWER_NONE;
}

void PowerUpManager::CreatePowerup(POWERTYPE getType, float getRelativeDegree, float getRelativeRadius)
{
	vecCrate.push_back(new Crate(&crateTexture, getType, getRelativeDegree, getRelativeRadius));
	//vecPowerUp.push_back(new PowerUp(&powerupTexture, getType, getRelativeDegree, getRelativeRadius));
}

void PowerUpManager::Draw(sf::RenderWindow * window)
{
	{
		auto it = vecPowerUp.begin();
		while (it != vecPowerUp.end())
		{
			window->draw(*(*it));
			it++;
		}
	}
	{
		auto it = vecCrate.begin();
		while (it != vecCrate.end())
		{
			window->draw(*(*it));
			++it;
		}
	}
}

POWERTYPE PowerUpManager::GetPowerUp()
{
	return storedPowerType;
}

void PowerUpManager::Update(float deltaTime)
{
	{
		auto it = vecPowerUp.begin();
		while (it != vecPowerUp.end())
		{
			(*it)->Update(deltaTime);
			miniMap->addDot((*it)->getPosition(), sf::Color(55, 100, 200, 200));
			if ((*it)->isDestroyed())//if the powerup is destroyed, delete it
			{
				delete (*it);
				it = vecPowerUp.erase(it);
				continue;
			}
			it++;
		}
	}
	{
		auto it = vecCrate.begin();
		while (it != vecCrate.end())
		{
			if ((*it)->isOpened())
			{
				(*it)->update(deltaTime);
			}
			else
			{
				(*it)->update(deltaTime);
				miniMap->addDot((*it)->getPosition(), sf::Color(55, 100, 200, 200));
				if ((*it)->isOpened())
				{
					vecPowerUp.push_back(new PowerUp(&powerupTexture, (*it)->getPowerupType(), (*it)->getAnglePos(), (*it)->getRadiusPos()));
				}
			}
			if ((*it)->isDestroyed())
			{
				delete (*it);
				it = vecCrate.erase(it);
			}
			else
			{
				++it;
			}
		}
	}
}

void PowerUpManager::StorePowerUp(Player* player) //store power up after the player grabs it
{
	if (storedPowerType == POWER_NONE)
	{
		auto it = vecPowerUp.begin();
		while (it != vecPowerUp.end())
		{
			if (CollisionManager::CheckCircles((*it)->getCollider(), player->getCollider()))
			{
				storedPowerType = (*it)->GetType();
				pickupSFX.play();
				firstPower = true;
				delete (*it);
				vecPowerUp.erase(it);
				return;
			}
			++it;
		}
	}
}

bool PowerUpManager::CheckFirstPower()
{
	return firstPower;
}
