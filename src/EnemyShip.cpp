#include "stdafx.h"
#include "EnemyShip.h"
#include "EnemyManager.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "Planet.h"

EnemyShip::EnemyShip(sf::Texture &acqTexture, sf::Texture &iceTexture, Planet* planet, SHIPTYPE getType, float getOffset, float getRelativeDegree, float getRelativeRadius)
{
	ptrSprite.setTexture(acqTexture);
	std::vector<sf::IntRect> ani;
	switch (getType)
	{
	case SHIP_TYPE1:
		ptrSprite.setTextureRect(sf::IntRect(181, 298, 179, 95));
		shield.setTexture(acqTexture);
		ani.push_back(sf::IntRect(196, 149, 194, 147));
		ani.push_back(sf::IntRect(196, 0, 194, 147));
		ani.push_back(sf::IntRect(0, 0, 194, 147));
		ani.push_back(sf::IntRect(0, 149, 194, 147));
		shield.setAnimationRects(ani);
		shield.setFrameRate(10);
		shield.setLoopEnabled(true);
		shield.setOrigin(97, 73.5);

		wiggleSpeed = 0.02;
		wiggleSize = 32;
		descentSpeed = 98;
		health = 2;
		cost = 400;
		isActive = true;

		break;
	case SHIP_TYPE2:
		ptrSprite.setTextureRect(sf::IntRect(148, 0, 146, 152));
		wiggleSpeed = 0.01;
		wiggleSize = 2;
		descentSpeed = 196;
		health = 1;
		cost = 200;
		isActive = true;

		break;
	case SHIP_TYPE3:
		ptrSprite.setTextureRect(sf::IntRect(0, 115, 176, 113));
		sprite2.setTexture(acqTexture);
		sprite2.setTextureRect(sf::IntRect(0, 115, 176, 113));
		wiggleSpeed = 0.01;
		wiggleSize = 2;
		descentSpeed = 98;
		health = 1;
		cost = 400;
		isActive = true;

		break;
	case SHIP_TYPE4:
		ptrSprite.setTextureRect(sf::IntRect(0, 115, 176, 113));
		wiggleSpeed = 0.01;
		wiggleSize = 2;
		descentSpeed = 98;
		health = 1;
		cost = 200;
		isActive = false;

		break;
	}
	EnemyShip::planet = planet;
	myType = getType;
	myOffset = getOffset;
	relativeAngle = getRelativeDegree;
	relativeRadius = getRelativeRadius;
	setPosition(cosf(relativeAngle)*relativeRadius, sinf(relativeAngle)*relativeRadius);
	auto size = ptrSprite.getLocalBounds();
	ptrSprite.setOrigin(size.width / 2, size.height / 2);
	ptrSprite.setPosition(getPosition());
	sprite2.setPosition(getPosition().x , getPosition().y );
	shield.setPosition(getPosition());

	iceCube.setTexture(iceTexture);
	std::vector<sf::IntRect> iceCubeFrames;
	iceCubeFrames.push_back(sf::IntRect(0, 328, 191, 162));
	iceCubeFrames.push_back(sf::IntRect(193, 0, 191, 162));
	iceCubeFrames.push_back(sf::IntRect(193, 164, 191, 162));
	iceCubeFrames.push_back(sf::IntRect(0, 164, 191, 162));
	iceCubeFrames.push_back(sf::IntRect(0, 0, 191, 162));
	iceCube.setAnimationRects(iceCubeFrames);
	iceCube.setLoopEnabled(false);
	iceCube.setFrameRate(10);
	iceCube.setRotation(rand() % 360);
	iceCube.setOrigin(95.5, 81);
}

EnemyShip::~EnemyShip()
{
}

void EnemyShip::Update(float dt)
{
	if (isActive == true)
	{
		if (!isFrozen && planet->getRadiusSize() + 32 < relativeRadius)
		{
			landed = false;
			relativeRadius -= descentSpeed * dt;
			float angleRadians = (relativeAngle - 90) * M_PI / 180;
			float dAngle = wiggleSize * sin(myOffset + relativeRadius * wiggleSpeed);
			relativeAngle += dAngle * dt;

			if (planet->isBordersEnabled())
			{
				float ccwBorder = planet->getCcwBorder();
				float cwBorder = planet->getCwBorder();
				if (relativeAngle < ccwBorder && relativeAngle < cwBorder)
				{
					relativeAngle = ccwBorder;
				}
				if (relativeAngle > cwBorder && relativeAngle > ccwBorder)
				{
					relativeAngle = cwBorder;
				}
			}

			angleRadians = (relativeAngle - 90) * M_PI / 180;
			setPosition(cos(angleRadians)*relativeRadius, sin(angleRadians)*relativeRadius);
			ptrSprite.setPosition(getPosition());
			ptrSprite.setRotation(relativeAngle);
			setRotation(relativeAngle);

			sprite2.setPosition(ptrSprite.getTransform().transformPoint(-136, -25));
			sprite2.setRotation(relativeAngle);
		}
		else if (isFrozen)
		{
			iceCube.updateAnimation(dt);
			freezeTimer -= dt;
			if (freezeTimer <= 0)
			{
				isFrozen = false;
			}
		}
		else
		{
			landed = true;
			switch (myType)
			{
			case SHIP_TYPE1:
				ptrSprite.setTextureRect(sf::IntRect(0, 298, 179, 95));
				break;
			case SHIP_TYPE2:
				ptrSprite.setTextureRect(sf::IntRect(0, 0, 146, 152));
				break;
			case SHIP_TYPE3:
				ptrSprite.setTextureRect(sf::IntRect(0, 0, 176, 113));
				sprite2.setTextureRect(sf::IntRect(0, 0, 176, 113));
				break;
			case SHIP_TYPE4:
				ptrSprite.setTextureRect(sf::IntRect(0, 0, 176, 113));
				break;
			case SHIP_TYPE5:
				break;
			default:
				break;
			}
		}
	}
	if (health >= 2 && myType == SHIP_TYPE1)
	{
		shield.setPosition(getPosition());
		shield.setRotation(getRotation());
		shield.updateAnimation(dt);
	}
}

void EnemyShip::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.draw(sprite2, states);
	target.draw(ptrSprite, states);
	if (health >= 2 && myType == SHIP_TYPE1 && !landed)
	{
		target.draw(shield, states);
	}
	if (isFrozen)
	{
		target.draw(iceCube, states);
	}
}

sf::CircleShape EnemyShip::getCollider()
{
	if (myType == SHIP_TYPE3)
	{
		sf::CircleShape collider;
		collider.setPosition(ptrSprite.getTransform().transformPoint(0, 0));
		collider.setRadius(ptrSprite.getLocalBounds().height);
		return collider;
	}
	else
	{
		sf::CircleShape collider;
		collider.setPosition(getPosition());
		collider.setRadius(ptrSprite.getLocalBounds().width / 2);
		return collider;
	}
}

float EnemyShip::GetRelativeAngle()
{
	return relativeAngle;
}

float EnemyShip::GetRelativeRadius()
{
	return relativeRadius;
}

SHIPTYPE EnemyShip::GetType()
{
	return myType;

}

void EnemyShip::hit(int damage)
{
	health -= damage;
}

void EnemyShip::destroy()
{
	if (health > 0)
	{
		health = 0;
	}
}

bool EnemyShip::isDestroyed()
{
	if (health > 0)
	{
		return false;
	}
	return true;
}

int EnemyShip::getCost()
{
	return cost;
}

bool EnemyShip::hasLanded()
{
	return landed;
}

void EnemyShip::Activate(float acqAngle, float acqRadius)
{
	isActive = true;
	relativeAngle = acqAngle;
	relativeRadius = acqRadius;
}

bool EnemyShip::IsActive()
{
	return isActive;
}

bool EnemyShip::IsFrozen()
{
	return isFrozen;
}

float EnemyShip::GetFreezeTimer()
{
	return freezeTimer;
}

void EnemyShip::SetFreezeTimer(float time)
{
	freezeTimer = time;
	isFrozen = true;
	iceCube.restartAnimation();
	iceCube.setPosition(getPosition());
}
