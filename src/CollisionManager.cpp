#include "CollisionManager.h"
#include <SFML/Graphics/CircleShape.hpp>

bool CollisionManager::CheckCircles(sf::CircleShape shape_a, sf::CircleShape shape_b)
{
	float delta_x = std::abs(shape_a.getPosition().x - shape_b.getPosition().x);
	float delta_y = std::abs(shape_a.getPosition().y - shape_b.getPosition().y);

	float dist = std::sqrt(delta_x*delta_x + delta_y*delta_y);

	dist -= (shape_a.getRadius() + shape_b.getRadius());

	if (dist <= 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
