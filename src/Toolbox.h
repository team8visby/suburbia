#pragma once
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

class Toolbox
{
public:
	Toolbox() {};
	~Toolbox() {};
	static sf::Vector2f lerp(sf::Vector2f currentPos, sf::Vector2f targetPos, float fraction, float dt);
	static float lerp(float currentPos, float targetPos, float fraction, float dt);
	static sf::RectangleShape line(sf::Vector2f pos1, sf::Vector2f pos2, float width);
};
