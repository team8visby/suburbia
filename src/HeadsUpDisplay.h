#pragma once
#include <SFML/Graphics/Drawable.hpp>
#include <string>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Clock.hpp>
#include <vector>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Text.hpp>
enum POWERTYPE;

class HeadsUpDisplay : public sf::Drawable
{
public:
	HeadsUpDisplay();
	~HeadsUpDisplay();
	void update(float marketValue, float dt);
	void setPowerup(POWERTYPE powerup);
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	void setWindowSize(sf::Vector2u size);
	void setScore(int score);
	void dmgNumber(int number);
	void setNextWaveClock(float seconds);
private:
	POWERTYPE currentPowerup;
	float marketValue;
	sf::Vector2f scale;
	sf::Texture hudTexture;
	sf::Sprite backgroundSprite;
	sf::Texture powerupTexture;
	sf::Sprite empSprite;
	sf::Sprite rocketsSprite;
	sf::Sprite freezeSprite;
	sf::Texture bossTexture;
	sf::Sprite bossSprite;
	sf::Texture turretTexture;
	sf::Sprite turretSprite;
	sf::Text scoreText;
	sf::Text nextWaveText;
	const int mvListSize;
	const float mvListUpdateRate;
	sf::Clock mvListUpdateClock;
	std::vector<float> mvList;
	std::vector<sf::RectangleShape> graph;
	std::vector<sf::Text> damageNumbers;
	sf::Font font;
	sf::Text mvText;
	sf::Text nextWaveCountdown;
	sf::Vector2u windowSize;
	float nextWaveClock;
};
