#include "stdafx.h"
#include "Gamestate.h"
#include "AudioManager.h"
#include "EnemyManager.h"
#include "PowerUpManager.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "MenuState.h"
#include "Toolbox.h"
#include "HighScoreState.h"

Gamestate::Gamestate(sf::RenderWindow* p_window) :
	ptrAudioManager(),
	ptrPowerUpManager(&map),
	buildingManager(&planet),
	ptrEnemyManager(&planet, &ptrAudioManager, &ptrPowerUpManager, &buildingManager, &map, &score),
	projectileManager(&ptrEnemyManager),
	player(&projectileManager, &planet, &map),
	turretManager(&projectileManager, &ptrEnemyManager, &map),
	objBoss()
{
	currentWave = -1;
	wavePause = false;
	window = p_window;
	sf::Vector2u windowSize = window->getSize();
	hud.setWindowSize(windowSize);
	objBoss.GetWindowSize(windowSize);


	reticleTexture.loadFromFile("../assets/Sprites/Reticle.png");
	reticle.setTexture(reticleTexture);

	float scale = windowSize.y / 1080;

	std::vector<sf::IntRect> gameOverFrames;
	gameOverFrames.push_back(sf::IntRect(0, 0, 1814, 1080));
	gameOverFrames.push_back(sf::IntRect(0, 2164, 1814, 1080));
	gameOverFrames.push_back(sf::IntRect(0, 1082, 1814, 1080));
	gameOverTexture.loadFromFile("../assets/Sprites/End_Screen.png");
	gameOverAnimation.setTexture(gameOverTexture);
	gameOverAnimation.setAnimationRects(gameOverFrames);
	gameOverAnimation.setScale(scale, scale);
	gameOverAnimation.setLoopEnabled(false);
	gameOverAnimation.setFrameRate(6);
	gameOverAnimation.setOrigin(907, 0);
	gameOverAnimation.setPosition(windowSize.x / 2.f, windowSize.y);
	gameOverTextTexture.loadFromFile("../assets/Sprites/YOUR'RE FIRED.png");
	gameOverText.setTexture(gameOverTextTexture);
	gameOverText.setScale(scale, scale);
	gameOverText.setOrigin(907, 540);
	gameOverText.setPosition(windowSize.x / 2.f, windowSize.y / 2.f);

	map.setPosition(windowSize.x * 0.89, windowSize.y * 0.84);
	map.setScale((windowSize.x / 1920.f) * 0.8, (windowSize.y / 1080.f) * 0.8);
	map.setSectorColor(1, sf::Color(50, 200, 50, 200));
	map.setSectorColor(2, sf::Color(100, 100, 100, 100));
	map.setSectorColor(3, sf::Color(100, 100, 100, 100));
	map.setSectorColor(4, sf::Color(100, 100, 100, 100));
	waveTimer = 0;
	score = 0;
	gameOver = false;
	quit = false;
	gameOverTimer = 5;
	//planet.setBordersEnabled(false);
}

void Gamestate::Enter()
{
	auto windowSize = window->getSize();
	buttonsTexture.loadFromFile("../assets/Key_prompt.png");
	leftButtonSprite.setTexture(buttonsTexture);
	rightButtonSprite.setTexture(buttonsTexture);
	aButtonSprite.setTexture(buttonsTexture);
	dButtonSprite.setTexture(buttonsTexture);
	spaceButtonSprite.setTexture(buttonsTexture);
	shiftButtonSprite.setTexture(buttonsTexture); 
	leftButtonSprite.setTextureRect(sf::IntRect(240, 0, 118, 226)); //Left Mouse Button
	rightButtonSprite.setTextureRect(sf::IntRect(120, 62, 118, 226)); //Right Mouse Button
	spaceButtonSprite.setTextureRect(sf::IntRect(0, 0, 212, 60)); //Space
	dButtonSprite.setTextureRect(sf::IntRect(166, 290, 60, 68)); //D
	aButtonSprite.setTextureRect(sf::IntRect(228,240,76,70)); //A
	shiftButtonSprite.setTextureRect(sf::IntRect(0,290,92,50)); //Shift
	
	shiftButton = new TutorialButton(sf::Vector2f(windowSize.x * 0.15, windowSize.y * 0.5), 0, shiftButtonSprite, sf::Keyboard::Key::LShift);
	aButton = new TutorialButton(sf::Vector2f(windowSize.x * 0.25, windowSize.y * 0.5), 0, aButtonSprite, sf::Keyboard::Key::A);
	dButton = new TutorialButton(sf::Vector2f(windowSize.x * 0.65, windowSize.y * 0.49), 0, dButtonSprite, sf::Keyboard::Key::D);
	spaceButton = new TutorialButton(sf::Vector2f(windowSize.x * 0.5, windowSize.y * 0.5), 0, spaceButtonSprite, sf::Keyboard::Key::Space);
	leftButton = new TutorialButton(sf::Vector2f(windowSize.x  * 0.25, windowSize.y * 0.2), 0, leftButtonSprite, sf::Mouse::Left);
	rightButton = new TutorialButton(sf::Vector2f(windowSize.x * 0.65, windowSize.y * 0.2), 0, rightButtonSprite, sf::Mouse::Right);
	spaceButton = new TutorialButton(sf::Vector2f(windowSize.x * 0.4, windowSize.y * 0.3), 0, spaceButtonSprite, sf::Keyboard::Key::Space);


	marketValue = 1000.f;

	planet.setPosition(0.f, 0.f);
	planet.setScale(1.44, 1.44);
	player.setRadiusPos(planet.getRadiusSize());
	sky.setPosition(0, 0);
	sky.setScale(1.35, 1.35);
	//camera.setSize(windowSize.x, windowSize.y);
	//camera.setSize(1280, 720);
	camera.setSize(1920, 1080);
	camera.zoom(1.2);
	reticle.setOrigin(55, 55);
	reticle.scale(0.7, 0.7);

	window->setMouseCursorVisible(false);

	buildingManager.CreateBuildings();

	player.Update(0.001);
	camera.setCenter(player.getPosition());
	camera.setRotation(player.getRotation());
	bossFlags = FLAG3;
	gameMusic.openFromFile("../assets/Sound Files/DST-TowerDefenseTheme.wav");
	gameMusic.setLoop(true);
	gameMusic.play();
	jingleBuffer.loadFromFile("../assets/Sound Files/Jingle_ver1.wav");
	jingleSFX.setBuffer(jingleBuffer);
	jingle1 = true;
	jingle2 = true;
	jingle3 = true;
}

void Gamestate::Exit()
{
	delete aButton;
	delete dButton;
	delete spaceButton;
	delete leftButton;
	delete rightButton;
}

bool Gamestate::Update(float dt)
{
	if (quit)
	{
		return false;
	}
	if (!gameOver && marketValue <= 0)
	{
		gameOver = true;
	}
	if (!gameOver)
	{
		marketValue += 10 * dt;
		if (waveTimer > 0)
		{
			waveTimer -= dt;
		}
	}
	else
	{
		gameOverTimer -= dt;
		gameOverAnimation.setPosition(
			gameOverAnimation.getPosition().x,
			Toolbox::lerp(
				gameOverAnimation.getPosition().y,
				0,
				0.1,
				dt));
		if (gameOverTimer <= 0)
		{
			gameOverAnimation.updateAnimation(dt);
		}
	}
	map.update(dt);
	hud.setPowerup(ptrPowerUpManager.GetPowerUp());
	hud.update(marketValue, dt);
	ptrPowerUpManager.Update(dt);
	player.Update(dt);
	projectileManager.Update(dt);
	ptrEnemyManager.Update(dt, marketValue, &hud);
	turretManager.Update(dt);
	planet.Update(dt);
	sky.Update(dt);
	ptrPowerUpManager.Update(dt);
	ptrPowerUpManager.StorePowerUp(&player);

	// Camera position
	float playerRotation = player.getRotation() - 90;
	float playerRadiusPos = player.getRadiusPos();
	float radiusOffset = 240;
	sf::Vector2f pos;
	pos.x = (playerRadiusPos + radiusOffset) * cos(playerRotation * M_PI / 180);
	pos.y = (playerRadiusPos + radiusOffset) * sin(playerRotation * M_PI / 180);

	{ // Camera lerp
		float fraction = .00001;
		camera.setCenter(Toolbox::lerp(camera.getCenter(), pos, fraction, dt));
		float cameraRot = camera.getRotation();
		float delta = cameraRot - (playerRotation + 90);
		float deltaLess = delta - 360;
		float deltaMore = delta + 360;
		if (std::min(std::abs(deltaLess), std::abs(deltaMore)) < std::abs(delta))
		{
			if (std::abs(deltaLess) < std::abs(deltaMore))
			{
				camera.setRotation(Toolbox::lerp(camera.getRotation() - 360, playerRotation + 90, fraction, dt));
			}
			else
			{
				camera.setRotation(Toolbox::lerp(camera.getRotation() + 360, playerRotation + 90, fraction, dt));
			}
		}
		else
		{
			camera.setRotation(Toolbox::lerp(camera.getRotation(), playerRotation + 90, fraction, dt));
		}
	}
	//camera.setCenter(pos);
	//camera.setRotation(playerRotation + 90);

	// Mouse to world transform
	auto mousePos = sf::Mouse::getPosition(*window);
	sf::Transform transf;
	transf.translate(camera.getCenter());
	transf.rotate(camera.getRotation());
	auto cameraSize = camera.getSize();
	transf.translate(-cameraSize.x / 2, -cameraSize.y / 2);
	auto windowSize = window->getSize();
	auto worldPos = transf.transformPoint(mousePos.x * (cameraSize.x / windowSize.x),
										  mousePos.y * (cameraSize.y / windowSize.y));
	player.setAim(worldPos);
	reticle.setPosition(sf::Mouse::getPosition().x, sf::Mouse::getPosition().y);

	hud.setPowerup(ptrPowerUpManager.GetPowerUp());
	buildingManager.Update(dt);
	objBoss.Update(dt);
	switch (bossFlags)
	{
	case FLAG1:
		if (buildingManager.GetDestroydBuildings() > 1 && talkClock.getElapsedTime().asSeconds() > 5)
		{
			objBoss.BossTalk(INFORMATIVE, "People are moving out! Do your job!", 5);
			bossFlags = FLAG2;
			talkClock.restart();
		}
		break;
	case FLAG2:
		if (buildingManager.GetDestroydBuildings() > 3 && talkClock.getElapsedTime().asSeconds() > 5)
		{
			objBoss.BossTalk(ANGRY, "You're fired!", 5);
			//bossFlags = FLAG3;
		}
		break;
	case FLAG3:
		if (bossClock.getElapsedTime().asSeconds() < 5)
		{
			if (objBoss.GetState() != TUTORIAL1)
			{
				objBoss.BossTalk(TUTORIAL1, "Move your cartank with the A and D keys", 5);
			}
		}
		if (bossClock.getElapsedTime().asSeconds() > 5 && bossClock.getElapsedTime().asSeconds() < 10)
		{
			if (objBoss.GetState() != TUTORIAL2)
			{
				objBoss.BossTalk(TUTORIAL2, "Aim with your mouse and shoot with the left mouse button", 5);
			}
		}
		if (bossClock.getElapsedTime().asSeconds() > 10 && bossClock.getElapsedTime().asSeconds() < 15)
		{
			if (objBoss.GetState() != TUTORIAL3)
			{
				objBoss.BossTalk(TUTORIAL3, "Set turrets with the space key, turrets cost 500 market value", 5);
				bossFlags = FLAG4;

			}
		}

		break;
	case FLAG4:
		if (bossClock.getElapsedTime().asSeconds() > 15 && ptrPowerUpManager.CheckFirstPower() == true)
		{
			if (objBoss.GetState() != TUTORIAL4)
			{
				objBoss.BossTalk(TUTORIAL4, "Use power ups with the right mouse button", 5);
				bossFlags = FLAG1;
				talkClock.restart();
			}
		}
		break;
	}

	// Wave stuff
	if (ptrEnemyManager.hasRoundEnded() && !gameOver)
	{
		if (!wavePause)
		{
			printf("Wave %i ended. \n", currentWave);
			waveTimer = 10;
			hud.setNextWaveClock(waveTimer);
			wavePause = true;
		}
		else if (waveTimer <= 0)
		{
			currentWave++;
			printf("Wave %i starting... \n", currentWave);
			wavePause = false;
			ptrEnemyManager.Initialize(currentWave);
		}
		else
		{
			int sectors = 1 + (currentWave + 1) / 2;
			if (sectors >= 4)
			{
				map.setSectorColor(4, sf::Color(200, 200, 50, 200));
				planet.setBordersEnabled(false);
				if (jingle3 == true)
				{
					jingleSFX.play();
					jingle3 = false;
				}
			}
			else
			{
				planet.setBorders(Toolbox::lerp(planet.getCwBorder(), 90 * sectors, .2, dt), 0);
				switch (sectors)
				{
				case 2:
					map.setSectorColor(2, sf::Color(200, 50, 50, 200));
					if (jingle1 == true)
					{
						jingleSFX.play();
						jingle1 = false;
						objBoss.BossTalk(INFORMATIVESHORT, "Hold LShift to move faster", 5);
					}
					break;
				case 3:
					map.setSectorColor(3, sf::Color(50, 50, 200, 200));
					if (jingle2 == true)
					{
						jingleSFX.play();
						jingle2 = false;
					}
					break;
				default:
					break;
				}
			}
		}
	}

	hud.setScore(score);

	return true;
}

void Gamestate::Draw(sf::RenderWindow* window)
{
	window->setView(camera); // Game world view
	window->draw(sky);
	buildingManager.Draw(window);
	window->draw(planet);
	ptrEnemyManager.Draw(window);
	turretManager.Draw(window);
	ptrPowerUpManager.Draw(window);
	projectileManager.Draw(window);
	window->draw(player);

	window->setView(window->getDefaultView()); // HUD view
	if (gameOver)
	{
		window->draw(gameOverAnimation);
		if (gameOverTimer <= -1.1)
		{
			window->draw(gameOverText);
		}
	}
	window->draw(*aButton);
	window->draw(*dButton);
	window->draw(*spaceButton);
	window->draw(*leftButton);
	window->draw(*rightButton);
	window->draw(*shiftButton);
	window->draw(hud);
	window->draw(objBoss);
	window->draw(map);
	window->draw(reticle);
}

void Gamestate::event(sf::Event e)
{
	if (window->hasFocus())
	{
		switch (e.type)
		{
		case sf::Event::KeyPressed:
			switch (e.key.code)
			{
			case sf::Keyboard::Escape:
				quit = true;
				break;
			case sf::Keyboard::Space:
				if (spaceButton->getShown() == true)
				{
					spaceButton->setShown(false);
				}
				turretManager.AddTurret(player.getPosition(), player.getRotation(), marketValue);
				if (marketValue >= 500)
				{
					hud.dmgNumber(-500);
				}
				break;
			case sf::Keyboard::A:
				if (aButton->getShown() == true)
				{
					aButton->setShown(false);
				}
				break;
			case sf::Keyboard::D:
				if (dButton->getShown() == true)
				{
					dButton->setShown(false);
				}
				break;
			case sf::Keyboard::LShift:
				if (shiftButton->getShown() == true)
				{
					shiftButton->setShown(false);
				}
				break;
			default:
				break;
			}
			break;

		case sf::Event::MouseButtonPressed:
			switch (e.mouseButton.button)
			{
			case sf::Mouse::Right:
				if (rightButton->getShown() == true)
				{
					rightButton->setShown(false);
				}
				if (ptrPowerUpManager.GetPowerUp() == POWER_ROCKETS)
				{
					ptrPowerUpManager.ClearPowerUp();
					projectileManager.AddProjectile(PROJ_MISSILE, player.getPosition(), player.getRotation() - 135);
					projectileManager.AddProjectile(PROJ_MISSILE, player.getPosition(), player.getRotation() - 95);
					projectileManager.AddProjectile(PROJ_MISSILE, player.getPosition(), player.getRotation() - 45);
				}
				else if (ptrPowerUpManager.GetPowerUp() == POWER_EMP)
				{
					ptrPowerUpManager.ClearPowerUp();
					turretManager.disableTurrets(8);
					projectileManager.AddProjectile(PROJ_EMP, player.getPosition(), player.getRotation());

					//Turns on a bool that shows an EMP field sprite around enemies and buildings, then clears the screen
					//Turn on a timer for the shut down buildings, turns buildings back on after set time.

				}
				else if (ptrPowerUpManager.GetPowerUp() == POWER_FREEZE)
				{
					ptrPowerUpManager.ClearPowerUp();
					projectileManager.AddProjectile(PROJ_FREEZE, player.getPosition(), player.getAim());
				}
				break;
			case sf::Mouse::Left:
				if (leftButton->getShown()==true)
				{
					leftButton->setShown(false);
				}
				break;
			default:
				break;
			}
			break;

		default:
			break;
		}
	}
}

IState * Gamestate::NextState()
{
	return new HighScoreState(score, window);
}
