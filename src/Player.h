#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Graphics/Texture.hpp>
#include "AnimatedSprite.h"

class MiniMap;
class HeadsUpDisplay;

namespace sf{
	class CircleShape;
}

class Planet;
class ProjectileManager;

class Player : public sf::Drawable, public sf::Transformable
{
public:
	Player(ProjectileManager* projMan, Planet* planet, MiniMap* map);
	~Player();
	void Update(float dt);
	void Fire();
	void setAim(float angle);
	void setAim(float xPos, float yPos);
	void setAim(sf::Vector2f aimPos);
	void setRadiusPos(float radius);
	void setAnglePos(float angle);
	float getAim();
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	float getRadiusPos();
	sf::CircleShape getCollider();
private:
	float radiusPos;
	float anglePos;
	float turretAngle;
	float direction;
	bool boosting;
	Planet* planet;
	MiniMap* miniMap;
	ProjectileManager* projectileManager;
	sf::Sprite carSprite;
	AnimatedSprite tankSprite;
	sf::Sprite cannonSprite;
	sf::Texture tankTexture;
	sf::IntRect tankAnimation[8];
	sf::Texture carTexture;
	float fireRateTimer;
	sf::SoundBuffer speedBuffer;
	sf::Sound speedSFX;
	bool speedPlay;
};
