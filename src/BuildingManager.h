#pragma once
#include "Building.h"
#include "Boss.h"

class Planet;
class EnemyManager;
class EnemyShip;
class Boss;

enum BUILDINGTYPE
{
	TYPE1,
	TYPE2,
	TYPE3,
	TYPE4,
	TYPE5,
	TYPE6,
	TYPE7,
	TYPE8,
	TYPE9,
	TYPE10
};

enum DAMAGESTATE
{
	NO_DAMAGE,
	DAMAGED
};

class BuildingManager
{
	Planet* ptrPlanet;
	std::vector<Building*> vecPtrBuildings;
	sf::Texture buildingTexture1;
	sf::Texture buildingTexture2;
	int destroyedBuildings;
public:
	BuildingManager( Planet* getPlanet);
	~BuildingManager();
	void CheckBuilding(EnemyShip* getEnemyShip);
	void Update(float deltaTime);
	void Draw(sf::RenderWindow* window);
	void CreateBuildings();
	void GetCameraPos(sf::View &acqCamera);
	int GetDestroydBuildings();
};