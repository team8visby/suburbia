#pragma once
#include "stdafx.h"
class SpriteManager;
//class DrawManager;
class InputManager; //May need to be different if we use different classes for input rather than just the one
namespace sf {
	class Window;
};

struct System
{
	int mem_intScreenWidth;
	int mem_intScreenHeight;
	SpriteManager* m_pxSpriteManager;
	// Not needed in this class anymore DrawManager* m_pxDrawManager;
	InputManager* m_pxInputManager;
};
class IState
{
public:
	virtual ~IState() {};
	virtual void Enter() {};
	virtual void event(sf::Event event) {};
	virtual bool Update(float p_fDeltaTime) = 0; //Code we need or not? Check with other teams or experienced SFLM users
	virtual void Exit() {};
	virtual void Draw(sf::RenderWindow* window) = 0;
	virtual IState* NextState() = 0;
};
