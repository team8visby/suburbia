#pragma once

namespace sf{
	class CircleShape;
}

class IEntity;

class CollisionManager
{
public:
	static bool CheckCircles(sf::CircleShape shape_a, sf::CircleShape shape_b);
private:
	CollisionManager() {};
	~CollisionManager() {};
};
