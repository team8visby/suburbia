#pragma once
#include "stdafx.h"
#include "AnimatedSprite.h"

enum BOSSSTATE;

class Boss: public sf::Drawable, public sf::Transformable
{
	bool bossTalk;
	sf::Clock myClock;
	sf::Sprite mySprite;
	sf::Texture myTexture;
	BOSSSTATE myType;
	sf::View cameraPos;
	sf::Font textFont;
	sf::Text objText;
	sf::SoundBuffer infoBuffer;
	sf::SoundBuffer infoShortBuffer;
	sf::SoundBuffer angryBuffer;
	sf::SoundBuffer angryShortBuffer;
	sf::Sound infoSFX;
	sf::Sound infoShortSFX;
	sf::Sound angrySFX;
	sf::Sound angryShortSFX;
	sf::Vector2u windowSize;
	sf::Vector2f scale;
	bool isTalking;
	float talkTime;
	sf::Texture speachBubbleTexture;
	sf::Sprite speachBubble;
	//std::vector<AnimatedSprite> bossAnimations;
	AnimatedSprite angryAnimation;
	sf::Texture angryTexture;
	sf::Texture angryStartTexture;
	sf::Texture angryEndTexture;
	AnimatedSprite angryStartAnimation;
	AnimatedSprite angryEndAnimation;
	sf::Clock aniClock;
	AnimatedSprite normalAnimation;
	AnimatedSprite happyAnimation;
	sf::Texture normalTexture;
	sf::Texture happyTexture;
public:
	Boss();
	~Boss();
	void Update(float deltaTime);
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	void RelativePosition(sf::View &acqCamera);
	void BossTalk(BOSSSTATE acqState, const sf::String (&acqText), float acqTalkTime);
	void GetWindowSize(sf::Vector2u(acqWindow));
	BOSSSTATE GetState();
};
