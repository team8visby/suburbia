#include "MenuButton.h"


MenuButton::MenuButton()
{
	collisionRect = sf::FloatRect(sf::Vector2f(0, 0), sf::Vector2f(100, 10));
	checkSprite.setPosition(collisionRect.left, collisionRect.top + collisionRect.height / 2);
	boxSprite.setPosition(collisionRect.left, collisionRect.top + collisionRect.height / 2);
	
	
}

MenuButton::MenuButton(char* bText, sf::Texture *menuTexture, sf::Vector2f pos, sf::Vector2f size)
{
	if (buttonFont.loadFromFile("../assets/{skinny} jeans solid.ttf"))
	{
		buttonText.setFont(buttonFont);
	}
	buttonText.setColor(sf::Color(0, 0, 0));
	hovered = false;
	collisionRect = sf::FloatRect(pos, size);
	boxSprite.setTexture(*menuTexture);
	boxSprite.setTextureRect(sf::IntRect(0, 2162, 115, 115));
	float scale = collisionRect.height / boxSprite.getLocalBounds().height;
	boxSprite.setScale(scale, scale);
	boxSprite.setOrigin(0, boxSprite.getLocalBounds().height / 2);
	boxSprite.setPosition(collisionRect.left, collisionRect.top + collisionRect.height / 2);
	checkSprite.setTexture(*menuTexture);
	checkSprite.setTextureRect(sf::IntRect(0, 2278, 101, 95));
	checkSprite.setScale(scale, scale);
	checkSprite.setOrigin(0, checkSprite.getLocalBounds().height / 2);
	checkSprite.setPosition(collisionRect.left, collisionRect.top + collisionRect.height / 2);
	
	buttonText.setString(bText);
	buttonText.setPosition(pos.x + size.x * 0.2, pos.y + size.y * 0.2);
	buttonText.setScale(scale * 2, scale * 2);
	//buttonText.setPosition(100, 100);
}

bool MenuButton::hover(sf::Vector2i mousePos)
{
	if (mousePos.x < collisionRect.width + collisionRect.left && mousePos.x > collisionRect.left &&
		mousePos.y < collisionRect.height + collisionRect.top && mousePos.y > collisionRect.top)
	{
		hovered = true;
		return true;
	}
	hovered = false;
	return false;
}

void MenuButton::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(boxSprite, states);
	if (hovered)
	{
		target.draw(checkSprite, states);
	}
	target.draw(buttonText, states);
}

MenuButton::~MenuButton()
{
}
