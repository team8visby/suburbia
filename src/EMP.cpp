#include "EMP.h"

EMP::EMP(sf::Vector2f pos, float rotation, EnemyManager* enemyManager, sf::Texture &projtext) : Projectile (pos, rotation, projtext)
{
	empShape.setPosition(pos);
	empShape.setRadius(10);
	empShape.setFillColor(sf::Color(0, 0, 0xFF, 126));
	EMP::enemyManager = enemyManager;
	empTexture.loadFromFile("../assets/Emp blast.png");
	empSprite.setTexture(empTexture);
	auto rect = empSprite.getLocalBounds();
	empSprite.setOrigin(rect.width / 2, rect.height / 2);
	empSprite.setPosition(pos);
	empSprite.setRotation(rotation);
	empSprite.setScale(10 / 956, 10 / 956);
}

void EMP::Update(float dt)
{
	empShape.setRadius(empShape.getRadius() + 12000 * dt);
	empSprite.setScale(empShape.getRadius() / 600, empShape.getRadius() / 600);
	empShape.setOrigin(empShape.getRadius(), empShape.getRadius());
	if (empShape.getRadius()>6000)
	{
		Destroy();
	}
}

void EMP::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(empSprite, states);
}

sf::CircleShape EMP::getCollider()
{
	return empShape;
}

PROJECTILETYPE EMP::GetProjectileType()
{
	return PROJ_EMP;
}

float EMP::GetTime()
{
	shutdownTime = empClock.getElapsedTime();
	return shutdownTime.asSeconds();
}
