#pragma once
#include "IState.h"
#include "MenuButton.h"
#include "Planet.h"
#include "Sky.h"

enum MENUSELECTION
{
	BUTTON_NONE,
	BUTTON_START,
	BUTTON_QUIT,
	BUTTON_HIGHSCORE
};

class MenuState : public IState
{
public:
	MenuState(sf::RenderWindow* window);
	void Enter();
	void Exit();
	bool Update(float dt);
	void Draw(sf::RenderWindow* window);
	IState* NextState();
	~MenuState();
private:
	sf::RenderWindow* window;
	sf::Sprite mainMenuSprite;
	sf::Sprite menuBockSprite;
	sf::Sprite menuBoxSprite;
	sf::Texture menuTexture;
	MenuButton* startButton;
	MenuButton* scoreButton;
	MenuButton* optionsButton;
	MenuButton* quitButton;
	Planet planet;
	Sky sky;
	MENUSELECTION selected;
	bool fullscreen;
	sf::Music menuMusic;
	sf::SoundBuffer buffer;
	sf::Sound scribbleSFX;
	bool scribblePlayed;
};
