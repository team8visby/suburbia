#include "Missile.h"
#include "EnemyShip.h"
#include "EnemyManager.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "Toolbox.h"

Missile::Missile(sf::Vector2f pos, float rotation, EnemyManager* enemyManager, sf::Texture &projtext): Projectile(pos, rotation, projtext)
{
	projectileSprite.setTexture(projtext);
	projectileSprite.setTextureRect(sf::IntRect(30, 0, 29, 85));
	projectileSprite.setOrigin(projectileSprite.getLocalBounds().width / 2, 0);
	projectileSprite.setPosition(pos);
	projectileSprite.setRotation(rotation + 90);
	Missile::enemyManager = enemyManager;
	homingDelayTimer = .2;
}

void Missile::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(projectileSprite, states);
}

void Missile::Update(float dt)
{
	homingDelayTimer -= dt;
	if (homingDelayTimer <= 0)
	{
		EnemyShip* enemyship = enemyManager->GetShipInRange(getPosition(), 64000);
		if (enemyship != nullptr)
		{
			float deltaX = getPosition().x - enemyship->getPosition().x;
			float deltaY = getPosition().y - enemyship->getPosition().y;
			float rotation = atan2(deltaY, deltaX) * 180 / M_PI + 180;
			float rotationLess = rotation - 360;
			float rotationMore = rotation + 360;
			if (std::min(std::abs(rotationLess - getRotation()), std::abs(rotationMore - getRotation())) < std::abs(rotation - getRotation()))
			{
				if (std::abs(rotationLess - getRotation()) < std::abs(rotationMore - getRotation()))
				{
					rotation = rotationLess;
				}
				else
				{
					rotation = rotationMore;
				}
			}
			setRotation(Toolbox::lerp(getRotation(), rotation, .0001, dt));
			projectileSprite.setRotation(getRotation() + 90);
		}
	}
	Projectile::Update(dt * 0.8);
	projectileSprite.setPosition(getPosition());
}

int Missile::getStrenght()
{
	return 2;
}

PROJECTILETYPE Missile::GetProjectileType()
{
	return PROJ_MISSILE;
}
