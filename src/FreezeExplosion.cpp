#include "FreezeExplosion.h"

FreezeExplosion::FreezeExplosion(sf::Vector2f pos, float rotation, EnemyManager* enemymanager, sf::Texture &projtext):  Projectile(pos, rotation, projtext)
{
	freezeShape.setPosition(pos);
	freezeShape.setRadius(993 / 2 - 10);
	freezeShape.setFillColor(sf::Color(0, 0, 0xFF, 126));
	this->enemyManager = enemyManager;
	freezeAnimation.setTexture(projtext);
	std::vector<sf::IntRect> freezeFrames;
	freezeFrames.push_back(sf::IntRect(995, 1990, 993, 993));
	freezeFrames.push_back(sf::IntRect(1990, 2985, 993, 993));
	freezeFrames.push_back(sf::IntRect(1990, 1990, 993, 993));
	freezeFrames.push_back(sf::IntRect(1990, 995, 993, 993));
	freezeFrames.push_back(sf::IntRect(1990, 0, 993, 993));
	freezeFrames.push_back(sf::IntRect(995, 2985, 993, 993));
	freezeFrames.push_back(sf::IntRect(2985, 0, 993, 993));
	freezeFrames.push_back(sf::IntRect(995, 995, 993, 993));
	freezeFrames.push_back(sf::IntRect(995, 0, 993, 993));
	freezeFrames.push_back(sf::IntRect(0, 2985, 993, 993));
	freezeFrames.push_back(sf::IntRect(0, 1990, 993, 993));
	freezeFrames.push_back(sf::IntRect(0, 995, 993, 993));
	freezeFrames.push_back(sf::IntRect(0, 0, 993, 993));
	freezeAnimation.setAnimationRects(freezeFrames);
	freezeAnimation.setFrameRate(30);
	freezeAnimation.setLoopEnabled(false);
	auto rect = freezeAnimation.getLocalBounds();
	freezeAnimation.setOrigin(rect.width / 2, rect.height / 2);
	freezeShape.setOrigin(rect.width / 2, rect.height / 2);
	freezeAnimation.setPosition(pos);
	freezeAnimation.setRotation(rand() % 360);
}

PROJECTILETYPE FreezeExplosion::GetProjectileType()
{
	return PROJ_FREEZPLOSION;
}

void FreezeExplosion::Update(float dt)
{
	if (freezeAnimation.isPlaybackFinished())
	{
		Destroy();
	}
	freezeAnimation.updateAnimation(dt);
}

void FreezeExplosion::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(freezeAnimation, states);
}

sf::CircleShape FreezeExplosion::getCollider()
{
	return freezeShape;
}
