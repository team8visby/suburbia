#include "stdafx.h"
#include "PowerUp.h"
#define _USE_MATH_DEFINES
#include <math.h>

PowerUp::PowerUp(sf::Texture * powerupTexture, int getType, float getRelativeDegree, float getRelativeRadius)
{
	myType = getType;
	relativeAngle = getRelativeDegree;//pos X
	relativeRadius = getRelativeRadius;//pos Y
	mySpeed = 64.f;
	ptrSprite.setTexture(*powerupTexture);
	switch (getType)
	{
	case POWER_ROCKETS:
		ptrSprite.setTextureRect(sf::IntRect(0, 46, 36, 47));
		break;
	case POWER_EMP:
		ptrSprite.setTextureRect(sf::IntRect(0, 0, 44, 44));
		break;
	case POWER_FREEZE:
		ptrSprite.setTextureRect(sf::IntRect(46, 0, 36, 53));
		break;
	case POWER_NONE:
	default:
		break;
	}
	ptrSprite.setOrigin(ptrSprite.getTextureRect().width / 2, ptrSprite.getTextureRect().height / 2);
	setPosition(cosf(relativeAngle)*relativeRadius, sinf(relativeAngle)*relativeRadius);
	ptrSprite.setPosition(cosf(relativeAngle)*relativeRadius, sinf(relativeAngle)*relativeRadius);
	ptrSprite.setScale(0.8, 0.8);

	float angleRadians = (relativeAngle - 90) * M_PI / 180;
	setPosition(cos(angleRadians)*relativeRadius, sin(angleRadians)*relativeRadius);
	setRotation(relativeAngle);
	ptrSprite.setPosition(getPosition());
	ptrSprite.setRotation(relativeAngle);
	ptrSprite.setScale(1.2, 1.2);
    //myRadius = ptrSprite current size / 2
	timeoutClock = 0;
	transparent = 0;
	destroyed = false;
	landed = false;
}

PowerUp::~PowerUp()
{

}

void PowerUp::Update(float dt)
{
	timeoutClock += dt;
	if (timeoutClock > 7)
	{
		transparent = ++transparent % 6;
		if (transparent < 3)
		{
			ptrSprite.setColor(sf::Color(255, 255, 255, 25));
		}
		else
		{
			ptrSprite.setColor(sf::Color(255, 255, 255, 255));
		}
		if (timeoutClock > 10)
		{
			destroy();
		}
	}
	ptrSprite.setOrigin(ptrSprite.getTextureRect().width / 2, ptrSprite.getTextureRect().height / 2 + sinf(timeoutClock * 3) * 2);
}

void PowerUp::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.draw(ptrSprite);
}

void PowerUp::destroy()
{
	destroyed = true;
}

bool PowerUp::isDestroyed()
{
	return destroyed;
}

float PowerUp::GetRadius()
{
	return relativeRadius;
}

sf::CircleShape PowerUp::getCollider()
{
	sf::CircleShape collider;
	collider.setPosition(getPosition());
	collider.setRadius(ptrSprite.getLocalBounds().height / 2);
	return collider;
}

POWERTYPE PowerUp::GetType()
{
	return POWERTYPE(myType);
}
