#pragma once
#include "stdafx.h"
#include "Projectile.h"
#include "EnemyManager.h"
#include "AnimatedSprite.h"

class FreezeExplosion: public Projectile
{
public:
	FreezeExplosion(sf::Vector2f pos, float rotation, EnemyManager* enemymanager, sf::Texture &projtext);
	PROJECTILETYPE GetProjectileType() override;
	void Update(float dt);
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	sf::CircleShape getCollider();
private:
	sf::Texture freezeTexture;
	AnimatedSprite freezeAnimation;
	EnemyManager* enemyManager;
	sf::CircleShape freezeShape;
};