#pragma once
#include "stdafx.h"
#include "Projectile.h"
#include "EnemyManager.h"

class FreezeShot : public Projectile
{
public:
	FreezeShot(sf::Vector2f pos, float rotation, EnemyManager* enemyManager, sf::Texture &projtext);
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	void Update(float dt) override;
	int getStrenght() override;
	PROJECTILETYPE GetProjectileType() override;
private:
	EnemyManager* enemymanager;
	AnimatedSprite freezeSprite;
};