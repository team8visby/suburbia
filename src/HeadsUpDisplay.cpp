#include "HeadsUpDisplay.h"
#include "Toolbox.h"
#include "PowerUpManager.h"

HeadsUpDisplay::HeadsUpDisplay(): mvListSize(24), mvListUpdateRate(0.4)
{
	currentPowerup = POWER_NONE;
	marketValue = 1000;
	scale.x = 1;
	scale.y = 1;
	font.loadFromFile("../assets/DJB Speak the Truth.ttf");
	mvText.setFont(font);
	hudTexture.loadFromFile("../assets/UIbakgrund.png");
	powerupTexture.loadFromFile("../assets/Sprites/UI_powers.png");
	empSprite.setTexture(powerupTexture);
	empSprite.setTextureRect(sf::IntRect(0, 0, 118, 117));
	rocketsSprite.setTexture(powerupTexture);
	rocketsSprite.setTextureRect(sf::IntRect(119, 0, 113, 112));
	freezeSprite.setTexture(powerupTexture);
	freezeSprite.setTextureRect(sf::IntRect(112, 118, 109, 109));
	backgroundSprite.setTexture(hudTexture);
	auto backGrSize = backgroundSprite.getGlobalBounds();
	backgroundSprite.setOrigin(backGrSize.width * 0.5, backGrSize.height);
	mvText.setColor(sf::Color(20, 20, 30));
	nextWaveClock = 0;
	nextWaveCountdown.setFont(font);
	nextWaveCountdown.setStyle(sf::Text::Bold);
	nextWaveCountdown.setColor(sf::Color(250, 250, 240));
	scoreText.setFont(font);
	scoreText.setColor(sf::Color(20, 20, 30));
	nextWaveText.setFont(font);
	nextWaveText.setColor(sf::Color(250, 250, 240));
	
	turretTexture.loadFromFile("../assets/Sprites/UI Turret.png");
	turretSprite.setTexture(turretTexture);
	turretSprite.setTextureRect(sf::IntRect(0, 154, 273, 152));
	setWindowSize(sf::Vector2u(1920, 1080));
}

HeadsUpDisplay::~HeadsUpDisplay()
{
}

void HeadsUpDisplay::setPowerup(POWERTYPE powerup)
{
	currentPowerup = powerup;
}

void HeadsUpDisplay::update(float p_marketValue, float dt)
{
	marketValue = p_marketValue;
	if (nextWaveClock > 0)
	{
		nextWaveClock -= dt;
		nextWaveCountdown.setString(std::to_string(static_cast<int>(nextWaveClock)));
		nextWaveCountdown.setOrigin(nextWaveCountdown.getCharacterSize() / 2, 0);
	}
	if (mvListUpdateClock.getElapsedTime().asSeconds() > mvListUpdateRate)
	{
		// Graph
		// ---------------
		float xOffset = 444;
		float fun = std::min(2100.f, marketValue - (rand() % 60) - 30);
		if (mvList.size() >= 1)
		{
			float stepX = windowSize.x;
			stepX *= 0.0035;
			float stepY = windowSize.y;
			stepY *= 0.00005;
			auto it = graph.begin();
			while (it != graph.end())
			{
				(*it).move(-stepX, 0);
				++it;
			}
			auto last = mvList.rbegin();
			sf::Vector2f scale = backgroundSprite.getScale();
			sf::Vector2f pos1 = sf::Vector2f(xOffset*scale.x + stepX * (mvListSize - 1), windowSize.y - 20 * scale.y - fun * stepY);
			sf::Vector2f pos2 = sf::Vector2f(xOffset*scale.x + stepX * mvListSize, windowSize.y - 20 * scale.y - (*last) * stepY);
			sf::RectangleShape line = Toolbox::line(pos1, pos2, 3);
			//printf("LinePos: x = %f, y = %f\n", line.getPosition().x, line.getPosition().y);
			if (marketValue < 500)
			{
				line.setFillColor(sf::Color(220, 20, 30));
			}
			else
			{
				line.setFillColor(sf::Color(20, 20, 30));
			}
			graph.push_back(line);
		}
		mvList.push_back(fun);
		while (mvList.size() > mvListSize)
		{
			graph.erase(graph.begin());
			mvList.erase(mvList.begin());
		}

		mvListUpdateClock.restart();
	}
	mvText.setString("$" + std::to_string(static_cast<int>(marketValue)));
	if (marketValue < 500)
	{
		mvText.setColor(sf::Color(220, 20, 30));
		turretSprite.setTextureRect(sf::IntRect(0, 0, 273, 152));
	}
	else
	{
		mvText.setColor(sf::Color(20, 20, 30));
		turretSprite.setTextureRect(sf::IntRect(0, 154, 273, 152));
	}
	{
		auto it = damageNumbers.begin();
		while (it != damageNumbers.end())
		{
			(*it).move(0, -80 * dt * (*it).getScale().y);
			sf::Color txtColor = (*it).getColor();
			txtColor.a -= std::min(static_cast<int>(std::max(15 * dt, 1.f)), static_cast<int>(txtColor.a));
			if (txtColor.a <= 0)
			{
				it = damageNumbers.erase(it);
				continue;
			}
			(*it).setColor(txtColor);
			++it;
		}
	}
}

void HeadsUpDisplay::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(backgroundSprite, states);
	target.draw(turretSprite, states);
	switch(currentPowerup)
	{
	case POWER_ROCKETS:
		target.draw(rocketsSprite, states);
		break;
	case POWER_EMP:
		target.draw(empSprite, states);
		break;
	case POWER_FREEZE:
		target.draw(freezeSprite, states);
		break;
	default:
		break;
	}
	target.draw(mvText, states);
	if (nextWaveClock > 0)
	{
		target.draw(nextWaveText, states);
		target.draw(nextWaveCountdown, states);
	}

	for (auto it = graph.begin(); it != graph.end(); ++it)
	{
		target.draw(*it, states);
	}
	for (auto it = damageNumbers.begin(); it != damageNumbers.end(); ++it)
	{
		target.draw(*it, states);
	}
	target.draw(scoreText, states);
}

void HeadsUpDisplay::setWindowSize(sf::Vector2u size)
{
	windowSize.x = size.x;
	windowSize.y = size.y;
	scale.x = windowSize.x / 1920.f;
	scale.y = windowSize.y / 1080.f;
	backgroundSprite.setPosition(windowSize.x * 0.5, windowSize.y * 1);
	mvText.setPosition(windowSize.x * 1, windowSize.y * 1);
	backgroundSprite.setScale(scale.x * 1, scale.y);
	empSprite.setPosition(windowSize.x * 0.517, windowSize.y * 0.87);
	empSprite.setScale(scale);
	rocketsSprite.setPosition(windowSize.x * 0.518, windowSize.y * 0.87);
	rocketsSprite.setScale(scale);
	freezeSprite.setPosition(windowSize.x * 0.518, windowSize.y * 0.87);
	freezeSprite.setScale(scale);
	turretSprite.setPosition(windowSize.x * .34, windowSize.y * .87);
	turretSprite.setScale(scale);
	mvText.setScale(scale.x * 0.8, scale.y * 0.8);
	mvText.setPosition(windowSize.x * 0.31, windowSize.y * 0.876);
	nextWaveCountdown.setScale(scale.x * 2, scale.y * 2);
	nextWaveCountdown.setPosition(windowSize.x * 0.5, windowSize.y * 0.1);
	scoreText.setScale(scale.x * 0.8, scale.y * 0.8);
	scoreText.setPosition(windowSize.x * .67, windowSize.y * .98);
	nextWaveText.setScale(scale.x * 2, scale.y * 2);
	nextWaveText.setPosition(windowSize.x * 0.5, windowSize.y * 0.04);
	nextWaveText.setString("Next wave in:");
	nextWaveText.setScale(scale.x * 2, scale.y * 2);
	nextWaveText.setOrigin(nextWaveText.getLocalBounds().width / 2, 0);
}

void HeadsUpDisplay::setScore(int score)
{
	scoreText.setString(std::to_string(score));
	auto rect = scoreText.getLocalBounds();
	scoreText.setOrigin(rect.width, rect.height / 2);
}

void HeadsUpDisplay::dmgNumber(int number)
{
	sf::Text txt;
	txt.setFont(font);
	if (number >= 0)
	{
		txt.setColor(sf::Color(10, 245, 10, 255));
		txt.setString("+$" + std::to_string(number));
	}
	else
	{
		txt.setColor(sf::Color(245, 10, 10, 255));
		txt.setString("-$" + std::to_string(number * -1));
	}
	sf::Vector2f scale = mvText.getScale();
	txt.setScale(scale.x * 1.4, scale.y * 1.4);
	txt.setPosition(mvText.getPosition());
	txt.move(0, -60 * scale.y);
	damageNumbers.push_back(txt);
}

void HeadsUpDisplay::setNextWaveClock(float seconds)
{
	nextWaveClock = seconds;
}
