#include "stdafx.h"
#include "StateManager.h"
#include "IState.h"
#include "MenuState.h"
#include "Gamestate.h"
#include "HighScoreState.h"

StateManager::StateManager(sf::RenderWindow* window)
{
	m_xClock.restart();
	m_pxCurrentState = new MenuState(window);
	m_pxCurrentState->Enter();
	m_xLastTick = m_xClock.getElapsedTime();
}


StateManager::~StateManager()
{
	//Checks if not a null state, if not, nulls and Exits
	if (m_pxCurrentState != nullptr)
	{
		m_pxCurrentState->Exit();
		delete m_pxCurrentState;
		m_pxCurrentState = nullptr;
	}
}

bool StateManager::Update()
{
	if (m_pxCurrentState != nullptr)
	{
		float dt = std::min(m_xClock.restart().asSeconds(), 1.f / 30.f);
		if (m_pxCurrentState->Update(dt) == false)
		{
			SetState(m_pxCurrentState->NextState());
		}
	}
	return true;
}

void StateManager::Draw(sf::RenderWindow* window)
{
	if (m_pxCurrentState != nullptr)
	{
		m_pxCurrentState->Draw(window);
	}
}

void StateManager::event(sf::Event keyEvent)
{
	if (m_pxCurrentState!=nullptr)
	{
		m_pxCurrentState->event(keyEvent);
	}
}

void StateManager::SetState(IState * p_xState)
{
	// If the state isn't nulled, run exit and null the state
	if (m_pxCurrentState != nullptr)
	{
		m_pxCurrentState->Exit();
		delete m_pxCurrentState;
	}
	// Sets the current state to be the state sent as paramemer and
	// calls the Enter function on the current state.
	m_pxCurrentState = p_xState;
	m_pxCurrentState->Enter();
}
