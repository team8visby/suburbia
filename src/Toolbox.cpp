#include "Toolbox.h"

#define _USE_MATH_DEFINES
#include <math.h>

sf::Vector2f Toolbox::lerp(sf::Vector2f currentPos, sf::Vector2f targetPos, float fraction, float dt)
{
	currentPos.x = lerp(currentPos.x, targetPos.x, fraction, dt);
	currentPos.y = lerp(currentPos.y, targetPos.y, fraction, dt);

	return currentPos;
}

float Toolbox::lerp(float current, float target, float fraction, float dt)
{
	fraction = 1 - powf(fraction, dt);
	return (current * (1.0f - fraction)) + (target * fraction);
}

sf::RectangleShape Toolbox::line(sf::Vector2f pos1, sf::Vector2f pos2, float width)
{
	float angle = -atan2(pos1.y - pos2.y, pos1.x - pos2.x) * 180 / M_PI;
	float length = sqrt(pow(pos1.x - pos2.x, 2) + pow(pos1.y - pos2.y, 2));
	sf::RectangleShape rect;
	rect.setOrigin(0, width / 2);
	rect.setSize(sf::Vector2f(length, width));
	rect.setRotation(angle);
	rect.setPosition(pos1);
	return rect;
}
