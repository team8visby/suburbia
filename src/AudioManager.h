#pragma once
/*#include <SFML\Audio\Sound.hpp>
#include <SFML\Audio\Music.hpp>
#include <SFML\Audio\SoundBuffer.hpp>*/
#include <map>


class AudioManager
{
public:
	AudioManager();
	~AudioManager();

	void Shutdown(); //Clears all containers

	sf::Sound* CreateSound(const std::string& ptrFilePath);
	//sf::Music* CreateMusic(const std::string& ptrFilePath);

	void DestroySound(const std::string& ptrFilePath);
	//void DestroyMusic(const std::string& ptrFilePath);

private:
	sf::Sound* ptrSound;
	//sf::Music* ptrMusic;

	sf::SoundBuffer soundBuffer;

	std::map<std::string, sf::Sound*> mapptrAudio;
	//std::map<std::string, sf::Music*> mapptrMusic;
};

