#pragma once
#include "IState.h"
#include "ProjectileManager.h"
#include "Player.h"
#include "Planet.h"
#include "EnemyManager.h"
#include "TurretManager.h"
#include "AudioManager.h"
#include "PowerUpManager.h"
#include "HeadsUpDisplay.h"
#include "Sky.h"
#include "BuildingManager.h"
#include "Boss.h"
#include "MiniMap.h"
#include "TutorialButton.h"

class EnemyShip;
class Player;
class EnemyManager;
class BuildingManager;

enum BOSSSTATE
{
	INFORMATIVE,
	ANGRY,
	TUTORIAL1,
	TUTORIAL2,
	TUTORIAL3,
	TUTORIAL4,
	INFORMATIVESHORT,
	ANGRYSHORT
};

enum BOSSFLAGS
{
	FLAG1,
	FLAG2,
	FLAG3,
	FLAG4
};

class Gamestate : public IState {
public:
	Gamestate(sf::RenderWindow* window);
	void Enter();
	void Exit();
	bool Update(float dt);
	void Draw(sf::RenderWindow* window);
	void event(sf::Event e);
	IState* NextState();
private:
	TutorialButton* aButton;
	TutorialButton* dButton;
	TutorialButton* spaceButton;
	TutorialButton* leftButton;
	TutorialButton* rightButton;
	TutorialButton* shiftButton;
	Planet planet;
	Sky sky;
	int currentWave;
	float waveTimer;
	float gameOverTimer;
	bool wavePause;
	bool gameOver;
	bool quit;
	int score;
	MiniMap map;
	AudioManager ptrAudioManager;
	PowerUpManager ptrPowerUpManager;
	EnemyManager ptrEnemyManager;
	ProjectileManager projectileManager;
	BuildingManager buildingManager;
	sf::Texture gameOverTexture;
	AnimatedSprite gameOverAnimation;
	sf::Texture gameOverTextTexture;
	sf::Sprite gameOverText;
	Player player;
	Boss objBoss;
	HeadsUpDisplay hud;
	TurretManager turretManager;
	sf::RenderWindow* window;
	sf::View camera;
	sf::Texture reticleTexture;
	sf::Sprite reticle;
	sf::Sprite aButtonSprite;
	sf::Sprite dButtonSprite;
	sf::Sprite leftButtonSprite;
	sf::Sprite rightButtonSprite;
	sf::Sprite spaceButtonSprite;
	sf::Sprite shiftButtonSprite;
	sf::Texture buttonsTexture;
	float marketValue;
	BOSSFLAGS bossFlags;
	sf::Music gameMusic;
	sf::Clock bossClock;
	sf::SoundBuffer jingleBuffer;
	sf::Sound jingleSFX;
	bool jingle1;
	bool jingle2;
	bool jingle3;
	sf::Clock talkClock;
};
