#pragma once
#include "stdafx.h"

class HeadsUpDisplay;
class AnimatedSprite;
class EnemyShip;
class Projectile;
class AudioManager;
class PowerUpManager;
class BuildingManager;
class Planet;
class MiniMap;

enum POWERTYPE;

enum SHIPTYPE
{
	SHIP_TYPE1,
	SHIP_TYPE2,
	SHIP_TYPE3,
	SHIP_TYPE4,
	SHIP_TYPE5,
	SHIPNOTRLLY_POWERUP
};

struct Spawn
{
	SHIPTYPE shipType;
	POWERTYPE powerType;
	float positionAngle;
	float spawnDelay;
};

class EnemyManager
{
	float spawnTimer;
	AudioManager* ptrAudioManager;
	PowerUpManager* ptrPowerUpManager;
	BuildingManager* ptrBuildingManager;
	std::vector<EnemyShip*> vecPtrEnemyShip;
	std::vector<AnimatedSprite> effects;
	sf::Texture iceCubeTexture;
	sf::Texture explosionTexture;
	std::vector<sf::IntRect> explosionFrames;
	sf::Texture dustTexture;
	std::vector<sf::IntRect> dustFrames;
	Spawn spawn;
	bool hasLanded;
	bool roundEnd;
	int shipsLanded;
	int* score;
	std::vector<Spawn> vecSpawnEnemyShip;
	Planet* planet;
	MiniMap* miniMap;
	sf::SoundBuffer explosionBuffer;
	sf::SoundBuffer floatingBuffer1;
	sf::SoundBuffer floatingBuffer2;
	sf::SoundBuffer floatingBuffer3;
	sf::SoundBuffer landedBuffer;
	sf::Sound explosionSFX;
	sf::Sound floatingSFX1;
	sf::Sound floatingSFX2;
	sf::Sound floatingSFX3;
	sf::Sound landedSFX;
	sf::Texture enemy1Texture;
	sf::Texture enemy2Texture;
	sf::Texture enemy3Texture;
	sf::SoundBuffer hitBuffer;
	sf::Sound hitSFX;
public:
	EnemyManager(Planet* planet, AudioManager* getPtrAudioManager, PowerUpManager* getPtrPowerUpManager, BuildingManager* acqBuildingManager, MiniMap * map, int* score);
	~EnemyManager();
	void Initialize(int wave);
	void CreateShip();
	void Update(float deltaTime, float& marketValue, HeadsUpDisplay * hud);
	EnemyShip* GetShipInRange(sf::Vector2f pos, float range);
	void IsHit(Projectile* proj);
	void Draw(sf::RenderWindow* window);
	bool hasRoundEnded();
	int ShipsLanded();
	std::vector<EnemyShip*> getShipVector();
	bool HasLanded();
	void CheckAudio();
};
