#pragma once
#include <vector>
#include "Turret.h"
#include "AnimatedSprite.h"

class MiniMap;
class HeadsUpDisplay;

namespace sf{
	class RenderWindow;
}

class EnemyManager;

class TurretManager
{
public:
	TurretManager(ProjectileManager* projManager , EnemyManager* enManager, MiniMap* map);
	~TurretManager();
	void AddTurret(sf::Vector2f pos, float rotation, float& marketValue);
	void Update(float dt);
	void Draw(sf::RenderWindow * window);
	void disableTurrets(float timeSec);
private:
	sf::Texture shockTexture;
	AnimatedSprite shockSprite;
	float disableTimer;
	std::vector<Turret*> turrets;
	EnemyManager* enemyManager;
	ProjectileManager* projectileManager;
	sf::SoundBuffer buffer;
	sf::Sound empSFX;
	sf::Clock sndLoop;
	sf::SoundBuffer spawnBuffer;
	sf::Sound spawnSFX;
	sf::SoundBuffer errorBuffer;
	sf::Sound errorSFX;
	MiniMap* miniMap;
	bool errPlay;
};
