#pragma once
#include "stdafx.h"
#include "EnemyManager.h"
#include "Projectile.h"

class EMP :public Projectile
{
public:
	EMP(sf::Vector2f pos, float rotation, EnemyManager* enemymanager,sf::Texture &projtext);
	PROJECTILETYPE GetProjectileType() override;
	float GetTime();
	void Update(float dt);
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	sf::CircleShape getCollider();
private:
	sf::Time shutdownTime;
	sf::Clock empClock;
	EnemyManager* enemyManager;
	sf::CircleShape empShape;
	sf::Texture empTexture;
	sf::Sprite empSprite;
};

