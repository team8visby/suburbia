#pragma once
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Clock.hpp>
#include <vector>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/Mouse.hpp>

class TutorialButton: public sf::Drawable
{
public:
	TutorialButton(sf::Vector2f pos, float rotation, sf::Sprite bSprite, sf::Keyboard::Key key);
	TutorialButton(sf::Vector2f pos, float rotation, sf::Sprite bSprite, sf::Mouse::Button);
	~TutorialButton();
	void Update(float dt);
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

	bool getShown();
	void setShown(bool shown);
private:
	bool hasBeenShown; // remove?
	bool isShown;
	sf::Mouse::Button assignedButton;
	sf::Keyboard::Key assignedKey;
	sf::Texture buttonTexture;
	sf::Sprite buttonSprite;
};
