#include "stdafx.h"
#include "Player.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "ProjectileManager.h"
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Window/Mouse.hpp>
#include "Planet.h"
#include "Toolbox.h"
#include "MiniMap.h"

Player::Player(ProjectileManager* projMan, Planet* planet, MiniMap* map)
{
	miniMap = map;
	Player::planet = planet;
	projectileManager = projMan;
	direction = 1;
	tankTexture.loadFromFile("../assets/Avatartank.png");
	std::vector<sf::IntRect> tankAni;
	tankAni.push_back(sf::IntRect(0, 0, 159, 62));
	tankAni.push_back(sf::IntRect(0, 63, 159, 62));
	tankAni.push_back(sf::IntRect(0, 126, 159, 62));
	tankAni.push_back(sf::IntRect(0, 189, 159, 62));
	tankAni.push_back(sf::IntRect(160, 0, 159, 62));
	tankAni.push_back(sf::IntRect(160, 63, 159, 62));
	tankAni.push_back(sf::IntRect(160, 126, 159, 62));
	tankAni.push_back(sf::IntRect(320, 0, 159, 62));
	carTexture.loadFromFile("../assets/Avatarcar.png");
	carSprite.setTexture(carTexture);
	carSprite.setTextureRect(sf::IntRect(0, 0, 231, 143));
	cannonSprite.setTexture(tankTexture);
	cannonSprite.setTextureRect(sf::IntRect(320, 63, 28, 98));
	tankSprite.setTexture(tankTexture);
	tankSprite.setAnimationRects(tankAni);
	tankSprite.setFrameRate(30);

	auto spriteSize = carSprite.getLocalBounds();
	sf::Vector2f spriteCenter;
	spriteCenter.x = spriteSize.width / 2;
	spriteCenter.y = spriteSize.height / 2;
	carSprite.setOrigin(spriteCenter);

	spriteSize = cannonSprite.getLocalBounds();
	spriteCenter.x = spriteSize.width / 2;
	spriteCenter.y = spriteSize.height;
	cannonSprite.setOrigin(spriteCenter);
	
	spriteSize = tankSprite.getLocalBounds();
	spriteCenter.x = spriteSize.width / 2;
	spriteCenter.y = spriteSize.height / 2;
	tankSprite.setOrigin(spriteCenter);

	radiusPos = planet->getRadiusSize() + 8;
	anglePos = 45;
	turretAngle = 0;

	speedBuffer.loadFromFile("../assets/Sound Files/Speed_ver1.wav");
	speedSFX.setBuffer(speedBuffer);
	speedPlay = true;
	boosting = false;
}

Player::~Player()
{
}

void Player::Update(float dt)
{
	fireRateTimer -= dt;
	float speed = std::max(20.f, std::abs(30 * direction));
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))
	{
		boosting = true;
		speed *= 2.4;
		if (speedPlay == true)
		{
			speedSFX.play();
			speedSFX.setLoop(true);
			speedPlay = false;
		}
	}
	if (!sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))
	{
		boosting = false;
		speedSFX.stop();
		speedSFX.setLoop(false);
		speedPlay = true;
	}
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left) ||
		sf::Keyboard::isKeyPressed(sf::Keyboard::LControl))
	{
		Fire();
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) ||
		sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		anglePos -= speed * dt;
		if(anglePos < -360)
		{
			anglePos += 360;
		}
		direction = std::min(1.f, direction + speed / 3 * dt);
		tankSprite.updateAnimation(dt);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D) ||
		sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		anglePos += speed * dt;

		if (anglePos >= 720)
		{
			anglePos -= 360;
		}
		direction = std::max(-1.f, direction - speed / 3 * dt);
		tankSprite.updateAnimation(-dt);
	}

	if (planet->isBordersEnabled())
	{
		float ccwBorder = planet->getCcwBorder() + 5;
		float cwBorder = planet->getCwBorder() - 5;
		if (anglePos < ccwBorder && anglePos < cwBorder)
		{
			anglePos = ccwBorder;
		}
		if (anglePos > cwBorder && anglePos > ccwBorder)
		{
			anglePos = cwBorder;
		}
	}

	float angleRadians = (anglePos - 90) * M_PI / 180;
	setPosition(cosf(angleRadians)*radiusPos, sinf(angleRadians)*radiusPos);

	setRotation(anglePos);
	tankSprite.setPosition(getPosition());
	tankSprite.setRotation(getRotation());
	carSprite.setScale(direction, 1);
	tankSprite.setScale(1, 1);
	cannonSprite.setScale(1, 1);

	angleRadians = (anglePos - 90) * M_PI / 180;
	cannonSprite.setPosition(cosf(angleRadians)*(radiusPos + 12), sinf(angleRadians)*(radiusPos + 6));

	angleRadians = (anglePos - 90 - 3.6 * direction) * M_PI / 180;
	carSprite.setPosition(cosf(angleRadians)*radiusPos, sinf(angleRadians)*radiusPos);
	carSprite.setRotation(anglePos - 4.5 * direction);

	auto spriteSize = cannonSprite.getLocalBounds();
	sf::Vector2f targetOrigin = sf::Vector2f(spriteSize.width / 2, spriteSize.height);
	cannonSprite.setOrigin(Toolbox::lerp(cannonSprite.getOrigin(), targetOrigin, .01, dt));

	miniMap->setRotation(-getRotation());
	miniMap->addDot(getPosition(), sf::Color(55, 200, 100, 230));
}

void Player::Fire()
{
	if (fireRateTimer <= 0 && !boosting)
	{
		fireRateTimer = .2;
		projectileManager->AddProjectile(PROJ_BULLET, cannonSprite.getPosition(), cannonSprite.getRotation() - 90);
		auto spriteSize = cannonSprite.getLocalBounds();
		sf::Vector2f newOrigin = sf::Vector2f(spriteSize.width / 2, spriteSize.height - 32);
		cannonSprite.setOrigin(newOrigin);
	}
}

void Player::setAim(float angle)
{
	//turretAngle = std::max(std::min(angle + 90, getRotation() + 100), getRotation() - 100);
	//cannonSprite.setRotation(turretAngle);
	turretAngle = angle + 90;
	cannonSprite.setRotation(turretAngle);
}

void Player::setAim(float xPos, float yPos)
{
	auto cannonPos = cannonSprite.getPosition();
	float deltaX = xPos - cannonPos.x;
	float deltaY = yPos - cannonPos.y;
	//printf("DeltaX: %f, DeltaY: %f\n", deltaX, deltaY);
	setAim(std::atan2(deltaY, deltaX) * 180 / M_PI);
}

void Player::setAim(sf::Vector2f aimPos)
{
	setAim(aimPos.x, aimPos.y);
}

void Player::setRadiusPos(float radius)
{
	radiusPos = radius;
}

void Player::setAnglePos(float angle)
{
	anglePos = angle;
}

float Player::getAim()
{
	return cannonSprite.getRotation()-90;
}

void Player::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(carSprite, states);
	target.draw(cannonSprite, states);
	target.draw(tankSprite, states);
}

float Player::getRadiusPos()
{
	return radiusPos;
}

sf::CircleShape Player::getCollider()
{
	sf::CircleShape collider;
	collider.setPosition(getPosition());
	collider.setRadius(carSprite.getLocalBounds().height / 2);
	return collider;
}
