#include "stdafx.h"
#include "StateManager.h"
// main.cpp

const int SCREENWIDTH = 1280;
const int SCREENHEIGHT = 720;

int main(int argc, char** argv) 
{
	//Creates and Renders a window (CLASS EXAMPLE CODE)
	sf::RenderWindow window;

	auto settings = window.getSettings();
	settings.antialiasingLevel = 4;

	//*
	window.create(sf::VideoMode(SCREENWIDTH, SCREENHEIGHT), "Planet Suburbia",
		sf::Style::Titlebar | sf::Style::Close, settings);
	/*/
	window.create(sf::VideoMode::getDesktopMode(), "Planet Suburbia",
		sf::Style::Fullscreen, settings);
	//*/
	if (!window.isOpen())
	{
		return -1;
	}


	StateManager* stateManager = new StateManager(&window);

	//window.setFramerateLimit(60);
	window.setVerticalSyncEnabled(true);
	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				window.close();
			}
			else
			{
				stateManager->event(event);
			}
		}

		stateManager->Update();
		window.clear(sf::Color(0x00, 0x00, 0x00, 0xff));
		stateManager->Draw(&window);

		window.display();
	}

	delete stateManager;
	return 0;
}
